#include "StdAfx.h"
#include "effect.h"
#include "image.h"
#include "animation.h"

effect::effect(void)
: _effectImage(NULL), 
_effectAnimation(NULL), 
_isRunning(false), 
_x(0), _y(0)
{
}

effect::~effect(void)
{
}

HRESULT effect::init(image* effectIamge, int frameW, int frameH, int fps, float elapsedTime)
{
	//이미지 활성화 여부
	_isRunning = false;

	//이미지 넣어준다
	_effectImage = effectIamge;

	//이펙트 경과시간
	_elapsedTime = elapsedTime;

	//이미지 애니메이션 객체가 없으면 생성
	if (!_effectAnimation) _effectAnimation = new animation;

	//애니메이션 정보 셋팅
	_effectAnimation->init(_effectImage->getWidth(), _effectImage->getHeight(), frameW, frameH);
	_effectAnimation->setDefPlayFrame(false, false);
	_effectAnimation->setFPS(fps);
	_effectAnimation->stop();

	return S_OK;
}

void effect::release(void)
{
	_effectImage = NULL;
	SAFE_DELETE(_effectAnimation);
}

void effect::render(void)
{
	//활성화 되지 않았다면 실행되지 않는다.
	if(!_isRunning) return;

	//EffectImage Animation Render
	_effectImage->aniRender(getMemDC(), _x, _y, _effectAnimation);
}

void effect::update(void)
{	
	//활성화 되지 않았다면 실행되지 않는다.
	if(!_isRunning) return;
	_effectAnimation->frameUpdate(_elapsedTime);

	//Effect Animaiton 이 종료 되었다면 Kill 한다.
	if(!_effectAnimation->isPlay()) killEffect();
}

//이펙트 시작
void effect::startEffect(int x, int y)
{
	//정보가 초기화 되어있지 않다면 실행되지 않는다.
	if(!_effectImage || !_effectAnimation) return;

	//매개 변수의 위치를 Effect 의 중앙으로 본다
	_x = x - (_effectAnimation->getFrameWidth() / 2);
	_y = y - (_effectAnimation->getFrameHeight() / 2);

	_isRunning = true;
	_effectAnimation->start();
}

//이펙트 살인
void effect::killEffect(void)
{
	_isRunning = false;
}
