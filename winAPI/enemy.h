#pragma once

#include "gameNode.h"

class enemy : public gameNode
{
private:
	image* _image;	//이미지를 담아둘 이미지 변수 선언
	RECT _rc;		//충돌 담당할 렉트 하나 생성

	int _currentFrameX; //현재 애니메이션 프레임 x
	int _currentFrameY; //현재 애니메이션 프레임 y
	
	int _rndTimeCount;
	int _count;

	int _rndFireCount;
	int _bulletFireCount; 

public:
	virtual HRESULT init(const char* imageName, POINT position);
	virtual void release(void);
	virtual void update(void);
	virtual void render(void);

	virtual void move(void);
	virtual void draw(void);
	virtual void animation(void);
	virtual bool bulletCountFire(void);

	//충돌에 필요한 렉트 접근자로 설정, 에너미 매니져로 보내준다
	inline RECT getRect(void) {return _rc;}

	enemy(void);
	virtual ~enemy(void);
};
