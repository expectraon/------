#pragma once

#include "singletonbase.h"
#include "timer.h"

//==============================================
//20140716 ## timeManager ##
//==============================================
class timeManager : public singletonBase <timeManager>
{
private:
	timer* _timer;

public:
	HRESULT init(void);
	void release(void);
	void update(float lock = 0.0f);
	void render(HDC hdc);

	//전체 시간 가져오기
	inline float getWorldTime(void) const {return _timer->getWorldTime();}

	//한프레임 경과시간
	inline float getElapsedTime(void) const {return _timer->getElapsedTime();}

	timeManager(void);
	~timeManager(void);
};
