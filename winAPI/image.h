#pragma once

#include "animation.h"

//==============================================
//20140701 ## 헤더파일 써준다~ ##
//==============================================
//이짱 10시간 잤음...

class image
{
public:
	//이미지 로딩 스타일~
	enum IMAGE_LOAD_KIND
	{
		LOAD_RESOURCE = 0,	//리소스로부터 로딩한다
		LOAD_FILE,			//파일로부터 로딩한다
		LOAD_EMPTY,			//그냥 생성한다(빈거~)
		LOAD_END
	};

	//이미지 정보 구조체
	typedef struct tagImageInfo
	{
		DWORD resID;
		HDC hMemDC;
		HBITMAP hBit;
		HBITMAP hOBit;
		float x;					//이미지 좌표 x
		float y;					//이미지 좌표 y
		int width;				//이미지 가로 크기
		int height;				//이미지 세로 크기
		int frameWidth;			//이미지 프레임 가로 크기
		int frameHeight;		//이미지 프레임 세로 크기
		int currentFrameX;		//현재프레임 X
		int currentFrameY;		//현재프레임 y
		int maxFrameX;			//프레임 최대치 x
		int maxFrameY;			//프레임 최대치 y
		BYTE loadType;
		
		tagImageInfo()
		{
			resID = 0;
			hMemDC = NULL;
			hBit = NULL;
			hOBit = NULL;
			x = 0;
			y = 0;
			width = 0;
			height = 0;
			frameWidth = 0;
			frameHeight = 0;
			currentFrameX = 0;
			currentFrameY = 0;
			maxFrameX = 0;
			maxFrameY = 0;
			loadType = LOAD_RESOURCE;
		}
	}IMAGE_INFO, *LPIMAGE_INFO;	

private:
	LPIMAGE_INFO _imageInfo;	//이미지 정보
	CHAR* _fileName;			//이미지 이름

	BOOL _tran;					//투명 배경 있냐?
	COLORREF _transColor;		//투명 컬러키

	BLENDFUNCTION	_blendFunc;		//알파 블렌드를 위한 정보
	LPIMAGE_INFO	_blendImage;	//알파 블렌드를 사용하기 위한 이미지 정보


public:
	image(void);
	~image(void);

	//빈 비트맵 만든다
	HRESULT init(int width, int height);

	//리소스로부터 읽는다
	HRESULT init(const DWORD resID, int width, int height, BOOL tran = FALSE,
		COLORREF transColor = RGB(0, 0, 0));

	//파일로부터 읽는다
	HRESULT init(const char* fileName, float x, float y, int width, int height, BOOL tran = FALSE,
		COLORREF transColor = RGB(0, 0, 0));

	HRESULT init(const char* fileName, int width, int height, BOOL tran = FALSE,
		COLORREF transColor = RGB(0, 0, 0));

	//이미지 + 프레임 파일 초기화
	HRESULT initFrame(const char* fileName, float x, float y, int width, int height, int frameX, int frameY,
		BOOL tran = FALSE, COLORREF transColor = RGB(0, 0, 0));

	HRESULT initFrame(const char* fileName, int width, int height, int frameX, int frameY,
		BOOL tran = FALSE, COLORREF transColor = RGB(0, 0, 0));

	//삭제
	void release(void);

	//투명키 셋팅
	void setTransColor(BOOL trans, COLORREF transColor);

	//일반렌더
	void render(HDC hdc);
	void render(HDC hdc, int destX, int destY);
	void render(HDC hdc, int sourX, int sourY, int sourWidth, int sourHeight);
	void render(HDC hdc, int destX, int destY, int sourX, int sourY, int sourwidth, int sourHeight);

	//알파 렌더
	void alphaRender(HDC hdc, int destX, int destY, BYTE alpha);
	void alphaRender(HDC hdc, int destX, int destY, int sourX, int sourY, int sourWidth, int sourHeight, BYTE alpha);

	//프레임렌더
	void frameRender(HDC hdc);
	void frameRender(HDC hdc, int destX, int destY);
	void frameRender(HDC hdc, int destX, int destY, int currentFrameX, int currentFrameY);

	//루프 렌더
	void loopRender(HDC hdc, const LPRECT drawArea, int offsetX, int offsetY);

	//애니 렌더
	void aniRender(HDC hdc, int destX, int destY, animation* ani);

	//DC얻기
	inline HDC getMemDC(void) {return _imageInfo->hMemDC;}

	//좌표 x
	inline void setX(float x) {_imageInfo->x = x;}
	inline float getX(void) {return _imageInfo->x;}

	//좌표 y
	inline void setY(float y) {_imageInfo->y = y;}
	inline float getY(void) {return _imageInfo->y;}

	//좌표 x, y
	inline void setCenter(float x, float y) {_imageInfo->x = x - (_imageInfo->width / 2);
								_imageInfo->y = y - (_imageInfo->height / 2);}

	inline float getCenterX(void) {return _imageInfo->maxFrameX <= 0 ? _imageInfo->x + (_imageInfo->width / 2) : _imageInfo->x + (_imageInfo->frameWidth / 2);}
	inline float getCenterY(void) {return _imageInfo->maxFrameY <= 0 ? _imageInfo->y + (_imageInfo->height / 2) : _imageInfo->y + (_imageInfo->frameHeight / 2);}
	
	inline void setFrameX(int frameX)
	{
		_imageInfo->currentFrameX = frameX;
		if (frameX > _imageInfo->maxFrameX) _imageInfo->currentFrameX = _imageInfo->maxFrameX;
	}
	inline int getFrameX(void) {return _imageInfo->currentFrameX;}

	inline void setFrameY(int frameY)
	{
		_imageInfo->currentFrameY = frameY;
		if (frameY > _imageInfo->maxFrameY) _imageInfo->currentFrameY = _imageInfo->maxFrameY;
	}
	inline int getFrameY(void) {return _imageInfo->currentFrameY;}

	inline int getMaxFrameX(void) {return _imageInfo->maxFrameX;}
	inline int getMaxFrameY(void) {return _imageInfo->maxFrameY;}

	inline int getFrameWidth(void) {return _imageInfo->frameWidth;}
	inline int getFrameHeight(void) {return _imageInfo->frameHeight;}

	inline int getWidth(void) {return _imageInfo->width;}
	inline int getHeight(void) {return _imageInfo->height;}

};
