#pragma once
#include "gameNode.h"

enum ANIMATIONDIRECTION
{
	ANIMATIONDIRECTION_NULL,
	ANIMATIONDIRECTION_X,
	ANIMATIONDIRECTION_Y
};

class frameAnimation : public gameNode
{
private:
	ANIMATIONDIRECTION _ad;

	image* _image;
	float _worldTimeCount;
	float _count;
	int _xFrame, _yFrame;
	int _xFrameCount;
	int _yFrameCount;

	bool _isPlay;
	bool _rewind;

public:
	void animationXFrame(image* img, int xFrame, float count, bool rewind);
	void animationYFrame(image* img, int yFrame, float count, bool rewind);

	virtual HRESULT init(void);
	virtual void release(void);
	virtual void update(void);
	virtual void render(void);

	inline bool getIsPlay(void){ return _isPlay; }

	frameAnimation();
	~frameAnimation();
};