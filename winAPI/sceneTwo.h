#pragma once
#include "gamenode.h"

class sceneTwo : public gameNode
{
private:
	char _text[128];
	int _y;

public:
	virtual HRESULT init(void);
	virtual void release(void);
	virtual void update(void);
	virtual void render(void);

	sceneTwo(void);
	virtual ~sceneTwo(void);
};
