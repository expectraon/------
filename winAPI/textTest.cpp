#include "StdAfx.h"
#include "textTest.h"

textTest::textTest(void)
{
}

textTest::~textTest(void)
{
}

HRESULT textTest::init(void)
{
	gameNode::init();


	return S_OK;
}

void textTest::release(void)
{
	gameNode::release();
}

void textTest::update(void)
{
	gameNode::update();
}

void textTest::render(void)
{
	char str[128];

	//일반텍스트
	//텍스트1
	sprintf(str, "반장 뭐 사올꺼임?!! *미중년 좋아잉~~*");
//	TextOut(getMemDC(), WINSIZEX / 2, WINSIZEY / 2, str, strlen(str));
/*
	//텍스트2
	/*
	HFONT font1;
	font1 = CreateFont(
		40,					//문자 사이즈
		0,					//문자 폭 (width) 0:기본값
		0,					//문자 기울기
		0,					//문자 방향
		400,				//문자 굵기//
		0,					//기울기
		0,					//밑줄
		0,					//취소선
		DEFAULT_CHARSET,	//문자셋
		0,					//출력정확도
		0,					//클리핑 정확도
		0,					//출력의질
		0,					//자간
		TEXT("Arial"));		//폰트
*/
	HFONT oldFont;
	HFONT font1;
	font1 = CreateFont(20, 0, 0, 0, 400, 0, 0, 0, HANGEUL_CHARSET, 0, 0, 0, 0, TEXT("궁서체"));
	oldFont = (HFONT)SelectObject(getMemDC(), font1);
	TextOut(getMemDC(), WINSIZEX / 2 - 200, WINSIZEY / 2, str, strlen(str));
	SelectObject(getMemDC(), oldFont);
	DeleteObject(font1);

	//텍스트3
	HFONT font2;
	RECT rcText = {100, 300, 300, 400};
	font2 = CreateFont(20, 0, 0, 0, FW_NORMAL, false, false, 0, DEFAULT_CHARSET, OUT_DEFAULT_PRECIS, CLIP_DEFAULT_PRECIS, PROOF_QUALITY, DEFAULT_PITCH, TEXT("궁서체"));
	oldFont = (HFONT)SelectObject(getMemDC(), font2);
	DrawText(getMemDC(), TEXT("반장 뭐 없음??"), 15, &rcText, DT_CENTER | DT_VCENTER | DT_SINGLELINE);
	SelectObject(getMemDC(), oldFont);
	DeleteObject(font2);
}
