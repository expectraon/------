#include "StdAfx.h"
#include "bullets.h"

bullet::bullet(void)
{
}

bullet::~bullet(void)
{
}

HRESULT bullet::init(const char* imageName, int bulletMax, float range)
{
	gameNode::init();

	_imageName = imageName;
	_range = range;
	_bulletMax = bulletMax;

	return S_OK;
}

void bullet::release(void)
{
	gameNode::release();
}

void bullet::update(void)
{
	gameNode::update();

	move();
}

void bullet::render(void)
{
	draw();
}

void bullet::fire(float x, float y, float angle, float speed)
{
	//최대 발사 갯수 제한
	if (_bulletMax < _vBullet.size()) return;

	tagBullet bullet;
	ZeroMemory(&bullet, sizeof(bullet));
	bullet.img = IMAGEMANAGER->findImage(_imageName);
	bullet.speed = speed;
	bullet.angle = angle;
	bullet.radius = bullet.img->getWidth() / 2;
	bullet.x = bullet.fireX = x;
	bullet.y = bullet.fireY = y;
	bullet.rc = RectMakeCenter(bullet.x, bullet.y,
			bullet.img->getWidth(), bullet.img->getHeight());
	_vBullet.push_back(bullet);
}

void bullet::move(void)
{
	for (_viBullet = _vBullet.begin(); _viBullet != _vBullet.end();)
	{
		_viBullet->x += cosf(_viBullet->angle) * _viBullet->speed;
		_viBullet->y += -sinf(_viBullet->angle) * _viBullet->speed;

		_viBullet->rc = RectMakeCenter(_viBullet->x, _viBullet->y,
			_viBullet->img->getWidth(), _viBullet->img->getHeight());

		//사거리 밖으로~~
		if (_range < getDistance(_viBullet->fireX, _viBullet->fireY,
			_viBullet->x, _viBullet->y))
		{
			_viBullet = _vBullet.erase(_viBullet);
		}
		else ++_viBullet;
	}
}

void bullet::draw(void)
{
	for (_viBullet = _vBullet.begin(); _viBullet != _vBullet.end(); ++_viBullet)
	{
		_viBullet->img->render(getMemDC(), _viBullet->rc.left, _viBullet->rc.top);
	}
}

void bullet::removeBullet(int arrNum)
{
	_vBullet.erase(_vBullet.begin() + arrNum);
}


missile::missile(void)
{
}

missile::~missile(void)
{
}

HRESULT missile::init(int bulletMax, float range)
{
	gameNode::init();

	_range = range;

	for (int i = 0; i < bulletMax; i++)
	{
		tagBullet bullet;
		ZeroMemory(&bullet, sizeof(bullet));
		bullet.img = new image;
		bullet.img->init("missile.bmp", 18, 64, true, RGB(255, 0, 255));
		bullet.speed = 1.2f;
		bullet.fire = false;
		_vBullet.push_back(bullet);
	}

	return S_OK;
}

void missile::release(void)
{
	gameNode::release();
}

void missile::update(void)
{
	gameNode::update();
	move();
}

void missile::render(void)
{
	draw();
}

//뽜이야~~ (정성룡 버젼)
void missile::fire(float x, float y)
{
	for (_viBullet = _vBullet.begin(); _viBullet != _vBullet.end(); ++_viBullet)
	{
		if (_viBullet->fire) continue;
		_viBullet->fire = true;
		_viBullet->x = _viBullet->fireX = x;
		_viBullet->y = _viBullet->fireY = y;
		_viBullet->rc = RectMakeCenter(_viBullet->x, _viBullet->y,
			_viBullet->img->getWidth(), _viBullet->img->getHeight());
		break;
	}
}

void missile::move(void)
{
	for (_viBullet = _vBullet.begin(); _viBullet != _vBullet.end(); ++_viBullet)
	{
		if (!_viBullet->fire) continue;
		_viBullet->y -= _viBullet->speed;
		_viBullet->rc = RectMakeCenter(_viBullet->x, _viBullet->y,
			_viBullet->img->getWidth(), _viBullet->img->getHeight());

		//사거리 밖으로~~
		if (_range < getDistance(_viBullet->fireX, _viBullet->fireY,
			_viBullet->x, _viBullet->y))
		{
			_viBullet->fire = false;
		}
	}
}

void missile::draw(void)
{
	for (_viBullet = _vBullet.begin(); _viBullet != _vBullet.end(); ++_viBullet)
	{
		if (!_viBullet->fire) continue;
		_viBullet->img->render(getMemDC(), _viBullet->rc.left, _viBullet->rc.top);
	}
}

missileM1::missileM1(void)
{
}

missileM1::~missileM1(void)
{
}

HRESULT missileM1::init(int bulletMax, float range)
{
	gameNode::init();

	_range = range;
	_bulletMax = bulletMax;

	return S_OK;
}

void missileM1::release(void)
{
	gameNode::release();
}

void missileM1::update(void)
{
	gameNode::update();
	move();
}

void missileM1::render(void)
{
	draw();
}

//뽜이야~~ (정성룡 버젼)
void missileM1::fire(float x, float y)
{
	//최대 발사 갯수 제한
	if (_bulletMax < _vBullet.size()) return;

	tagBullet bullet;
	ZeroMemory(&bullet, sizeof(bullet));
	bullet.img = new image;
	bullet.img->init("missile.bmp", 18, 64, true, RGB(255, 0, 255));
	bullet.speed = 180.2f;
	bullet.x = bullet.fireX = x;
	bullet.y = bullet.fireY = y;
	bullet.rc = RectMakeCenter(bullet.x, bullet.y,
			bullet.img->getWidth(), bullet.img->getHeight());
	_vBullet.push_back(bullet);
}

void missileM1::move(void)
{
	for (_viBullet = _vBullet.begin(); _viBullet != _vBullet.end();)
	{
		//초당 _viBullet->speed만큼~~~
		float elpasedTime = TIMEMANAGER->getElapsedTime();
		float moveSpeed = elpasedTime * _viBullet->speed;
		_viBullet->y -= moveSpeed;
		_viBullet->rc = RectMakeCenter(_viBullet->x, _viBullet->y,
			_viBullet->img->getWidth(), _viBullet->img->getHeight());

		//사거리 밖으로~~
		if (_range < getDistance(_viBullet->fireX, _viBullet->fireY,
			_viBullet->x, _viBullet->y))
		{
			SAFE_DELETE(_viBullet->img);
			_viBullet = _vBullet.erase(_viBullet);
		}
		else ++_viBullet;
	}
}

void missileM1::draw(void)
{
	for (_viBullet = _vBullet.begin(); _viBullet != _vBullet.end(); ++_viBullet)
	{
		_viBullet->img->render(getMemDC(), _viBullet->rc.left, _viBullet->rc.top);
	}
}

void missileM1::removeMissile(int arrNum)
{
	SAFE_DELETE(_vBullet[arrNum].img);
	_vBullet.erase(_vBullet.begin() + arrNum);
}


