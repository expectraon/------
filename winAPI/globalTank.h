#pragma once

#define TILESIZE 32
#define TILEX 20		//뿌릴 가로 타일 갯수
#define TILEY 20		//뿌릴 세로 타일 갯수
#define TILESIZEX TILESIZE * TILEX  //타일 가로 전체 사이즈
#define TILESIZEY TILESIZE * TILEY	//타일 세로 전체 사이즈

#define SAMPLETILEX 20		//샘플 타일 x
#define SAMPLETILEY 9		//샘플 타일 y

#define ATTR_UNMOVAL 0x0000001
#define ATTR_POISON 0x0000002

using namespace std;

enum TERRAIN
{
	TR_CEMENT, TR_EARTH, TR_GLASS, TR_WATER
};

enum OBJECT
{
	OBJ_BLOCK1, OBJ_BLOCK3, OBJ_BLOCKS,
	OBJ_TANK1, OBJ_TANK2, OBJ_NONE
};

enum POS
{
	POS_TANK1, POS_TANK2
};

struct tagMap
{
	TERRAIN terrain;
	OBJECT obj;
	RECT rcTile;
	int terrainFrameX;
	int terrainFrameY;
	int objFrameX;
	int objFrameY;
};

struct tagSampleMap
{
	RECT rcTile;
	int terrainFrameX;
	int terrainFrameY;
};

struct tagCurrentTile
{
	int x;
	int y;
};