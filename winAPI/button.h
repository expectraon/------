#pragma once

#include "gamenode.h"

typedef void (*CALLBACK_FUNCTION) (void);

enum BUTTONDIRECTION
{
	BUTTONDIRECTION_NULL,
	BUTTONDIRECTION_UP,
	BUTTONDIRECTION_DOWN
};

class button : public gameNode
{
private:
	BUTTONDIRECTION _direction;
	const char* _imageName;
	image* _image;

	RECT _rc;
	float _x, _y;

	POINT _btnDownFramePoint;
	POINT _btnUpFramePoint;

	//콜백 함수 원형 선언
	CALLBACK_FUNCTION _callbackFunction;

public:
	virtual HRESULT init(const char* imageName,  int x, int y,
		POINT btnDownFramePoint, POINT btnUpFramePoint, CALLBACK_FUNCTION cbFunction);
	virtual void release(void);
	virtual void update(void);
	virtual void render(void);

	button(void);
	~button(void);
};
