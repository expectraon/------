﻿#pragma once

#include "singletonBase.h"
#include <time.h>

class randomFunction : public singletonBase <randomFunction>
{
public:
	randomFunction(void)
	{
//		srand((unsigned int)time(NULL));
		srand(GetTickCount());
	}
	~randomFunction(void)
	{
	}

	//getInt
	int getInt(int num) {return rand() % num;}
	int getFromIntTo(int fromNum, int toNum) {return (rand() % (toNum - fromNum + 1)) + fromNum;}

	//getfloat
	//0.0f ~ 1.0f RAND_MAX  rand함수의 최대값 (32767)
	float getFloat() {return (float)rand() / RAND_MAX;}

	float getFloat(float num) {return ((float)rand() / (float)RAND_MAX) * num;}
	float getFromFloatTo(float fromFloat, float toFloat)
	{
		float rnd = (float)rand() / (float)RAND_MAX;
		return (rnd * (toFloat - fromFloat) + fromFloat);
	};
};