#pragma once
#include "gamenode.h"
#include "button.h"

class soundTestScene : public gameNode
{
private:
	button* _btnPlay;
	button* _btnStop;
	button* _btnPause;

public:
	virtual HRESULT init(void);
	virtual void release(void);
	virtual void update(void);
	virtual void render(void);

	static void cbPlay(void);
	static void cbStop(void);
	static void cbPause(void);

	soundTestScene(void);
	virtual ~soundTestScene(void);
};
