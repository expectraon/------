#include "StdAfx.h"
#include "pixelCollisionScene.h"

pixelCollisionScene::pixelCollisionScene(void)
{
}

pixelCollisionScene::~pixelCollisionScene(void)
{
}

HRESULT pixelCollisionScene::init(void)
{
	gameNode::init();
	
	_pc = new pixelCollision;
	_pc->init();

	return S_OK;
}

void pixelCollisionScene::release(void)
{
	gameNode::release();

	SAFE_DELETE(_pc);
}

void pixelCollisionScene::update(void)
{
	gameNode::update();

	_pc->update();
}

void pixelCollisionScene::render(void)
{
	_pc->render();
}

