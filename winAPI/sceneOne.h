#pragma once

#include "gamenode.h"
#include "button.h"

class sceneOne : public gameNode
{
private:
	char _text[128];
	button* _button;

public:
	virtual HRESULT init(void);
	virtual void release(void);
	virtual void update(void);
	virtual void render(void);

	static void nextScene(void);

	sceneOne(void);
	virtual ~sceneOne(void);
};
