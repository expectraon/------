#pragma once
#include "singletonbase.h"

class txtDataManager : public singletonBase<txtDataManager>
{
public:
	virtual HRESULT init(void);
	virtual void release(void);
	virtual void update(void);
	virtual void render(void);

	//¼��
	void txtSave(const char* saveFile, vector<string> vStr);
	char* vectorArrayCombine(vector<string> vArray);

	//��������
	vector<string> txtLoad(const char* loadFileName);
	vector<string> charArraySeparation(char charArray[]);

	txtDataManager(void);
	~txtDataManager(void);
};
