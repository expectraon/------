#include "StdAfx.h"
#include "aStarTest.h"

aStarTest::aStarTest(void)
{
}

aStarTest::~aStarTest(void)
{
}

HRESULT aStarTest::init(void)
{
	gameNode::init();

	//타일 셋팅
	setTiles();

	return S_OK;
}

void aStarTest::release(void)
{
	gameNode::release();
}

void aStarTest::update(void)
{
	gameNode::update();

	if (KEYMANAGER->isOnceKeyDown(VK_SPACE))
	{
		pathFinder(_currentTile);
	}

	if (KEYMANAGER->isStayKeyDown(VK_LBUTTON))
	{
		for (int i = 0; i < _vTotalList.size(); i++)
		{
			if (PtInRect(&_vTotalList[i]->getRect(), _ptMouse))
			{
				_vTotalList[i]->setColor(RGB(0, 145, 201));
				_vTotalList[i]->setAttribute("wall");
				break;
			}
		}
	}

	for (int i = 0; i < _vTotalList.size(); i++)
	{
		_vTotalList[i]->update();
	}
}

void aStarTest::render(void)
{
	for (int i = 0; i < _vTotalList.size(); i++)
	{
		_vTotalList[i]->render();
	}
}

void aStarTest::pathFinder(tile* currentTile)
{
	float tempTotalCost = 5000;
	tile* tempTile;

	for (int i = 0; i < addOpenList(_currentTile).size(); i++)
	{
		//해당 노드에서 목표지점까지의 이동력 계산
		_vOpenList[i]->setCostToGoal((abs(_endTile->getIdX() - _vOpenList[i]->getIdX()) + 
			abs(_endTile->getIdY() - _vOpenList[i]->getIdY())) * 10);

		//현재 노드에서 해당노드까지의 이동력 계산
		POINT center1 = _vOpenList[i]->getParentNode()->getCenter();
		POINT center2 = _vOpenList[i]->getCenter();
		_vOpenList[i]->setCostFromStart(((getDistance(center1.x, center1.y, center2.x, center2.y) > TILEWIDTH) ? 14 : 10));

		//총 이동력 계산
		_vOpenList[i]->setTotalCost(_vOpenList[i]->getCostToGoal() + _vOpenList[i]->getCostFromStart());

		//가장 가까운넘 뽑는다
		if (tempTotalCost > _vOpenList[i]->getTotalCost())
		{
			tempTotalCost = _vOpenList[i]->getTotalCost();
			tempTile = _vOpenList[i];
		}

		//갈 수 있는 길은 모두 오픈리스트에 넣어둔다
		bool addObj = true;
		for (_viOpenList = _vOpenList.begin(); _viOpenList != _vOpenList.end(); ++_viOpenList)
		{
			if (*_viOpenList == tempTile)
			{
				addObj = false;
				break;
			}
		}

		_vOpenList[i]->setIsOpen(false);
		if (!addObj) continue;
		_vOpenList.push_back(tempTile);
	}

	//목적지에 다다랐으면 리턴~
	if (tempTile->getAttribute() == "end")
	{
		while (_currentTile->getParentNode() != NULL)
		{
			_currentTile->setColor(RGB(22, 14, 128));
			_currentTile = _currentTile->getParentNode();
		}

		return;
	}

	//현재 타일은(지나온길) 클로즈 리스트에 넣는다
	_vCloseList.push_back(tempTile);

	//다음 노드로 설정한 타일을 오픈리스트에서 제거한다
	for (_viOpenList = _vOpenList.begin(); _viOpenList != _vOpenList.end(); ++_viOpenList)
	{
		if (*_viOpenList == tempTile)
		{
			_viOpenList = _vOpenList.erase(_viOpenList);
			break;
		}
	}

	//현재 타일에 템프타일(제일 가까운 길로 고른 타일) 넣는다
	_currentTile = tempTile;

	//다음 노드 찾는다
	pathFinder(_currentTile);
}

vector<tile*> aStarTest::addOpenList(tile* currentTile)
{
	int startX = currentTile->getIdX() - 1;
	int startY = currentTile->getIdY() - 1;

	//현재 노드에서 주변을 확장하면서 검사
	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 3; j++)
		{
			tile* node = _vTotalList[(startY * TILENUMX) + startX + j + (i * TILENUMX)];
			if (!node->getIsOpen()) continue;
			if (node->getAttribute() == "start") continue;
			if (node->getAttribute() == "wall") continue;

			node->setParentNode(currentTile);

			//갈수 있는 곳은 모두 오픈리스트에 넣어둔다
			bool addObj = true;
			for (_viOpenList = _vOpenList.begin(); _viOpenList != _vOpenList.end(); ++_viOpenList)
			{
				if (*_viOpenList == node)
				{
					addObj = false;
					break;
				}
			}

			//검사하는 곳 길 표시..
			if (node->getAttribute() != "end") node->setColor(RGB(128, 64, 22));
			
			if (!addObj) continue;
			_vOpenList.push_back(node);
		}
	}

	return _vOpenList;
}

void aStarTest::setTiles(void)
{
	//시작타일
	_startTile = new tile;
	_startTile->init(4, 4);
	_startTile->setAttribute("start");

	//목적지 타일
	_endTile = new tile;
	_endTile->init(20, 20);
	_endTile->setAttribute("end");

	//현재타일을 시작타일로 설정
	_currentTile = _startTile;

	//타일 초기화
	for (int i = 0; i < TILENUMY; i++)
	{
		for (int j = 0; j < TILENUMX; j++)
		{
			//시작타일
			if (j == _startTile->getIdX() && i == _startTile->getIdY())
			{
				_startTile->setColor(RGB(0, 255, 255));
				_vTotalList.push_back(_startTile);
				continue;
			}

			//목적지 타일
			if (j == _endTile->getIdX() && i == _endTile->getIdY())
			{
				_endTile->setColor(RGB(255, 0, 255));
				_vTotalList.push_back(_endTile);
				continue;
			}

			tile* node = new tile;
			node->init(j, i);
			_vTotalList.push_back(node);
		}
	}
}
