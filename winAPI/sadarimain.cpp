#include "stdafx.h"
#include "sadarimain.h"


sadarimain::sadarimain()
{
}


sadarimain::~sadarimain()
{
}

HRESULT sadarimain::init(void)
{
	gameNode::init();
	IMAGEMANAGER->addImage("bg1", "img/BackGround/bg1.bmp", 958, 319, true, RGB(255, 0, 255));

	//IMAGEMANAGER->addFrameImage("P-01", "img/P-01.bmp", 21, 56, 1, 3, true, RGB(255, 0, 255));

	IMAGEMANAGER->addFrameImage("ammo_boom1", "img/EFFECT/ammo_boom1.bmp", 70, 360, 1, 5, true, RGB(255, 0, 255));
	IMAGEMANAGER->addFrameImage("ammo_boom2", "img/EFFECT/ammo_boom2.bmp", 100, 700, 1, 7, true, RGB(255, 0, 255));
	IMAGEMANAGER->addFrameImage("boom_1", "img/EFFECT/boom_1.bmp", 100, 445, 1, 5, true, RGB(255, 0, 255));
	IMAGEMANAGER->addFrameImage("exp_big", "img/EFFECT/exp_big.bmp", 150, 1500, 1, 10, true, RGB(255, 0, 255));


	IMAGEMANAGER->addFrameImage("Npc1", "img/Npc/Npc1/Npc1.bmp", 50, 250, 1, 5, true, RGB(255, 0, 255));
	IMAGEMANAGER->addFrameImage("Npc1_attack", "img/Npc/Npc1/Npc1_attack.bmp", 112, 424, 1, 8, true, RGB(255, 0, 255));

	IMAGEMANAGER->addFrameImage("Npc8", "img/npc/npc8/Npc8.bmp", 55, 245, 1, 3, true, RGB(255, 0, 255));
	IMAGEMANAGER->addFrameImage("npc8_ammo", "img/npc/npc8/npc8_ammo.bmp", 55, 245, 1, 3, true, RGB(255, 0, 255));
	IMAGEMANAGER->addFrameImage("Npc8_attack", "img/npc/npc8/Npc8_attack.bmp", 75, 150, 1, 3, true, RGB(255, 0, 255));

	IMAGEMANAGER->addFrameImage("npc14", "img/npc/npc14/npc14.bmp", 38, 255, 1, 3, true, RGB(255, 0, 255));
	IMAGEMANAGER->addFrameImage("npc14_attack", "img/npc/npc14/npc14_attack.bmp", 42, 728, 1, 3, true, RGB(255, 0, 255));

	IMAGEMANAGER->addFrameImage("npc16", "img/npc/npc16/npc16.bmp", 49, 126, 1, 3, true, RGB(255, 0, 255));
	IMAGEMANAGER->addFrameImage("npc16_attack", "img/npc/npc16/npc16_attack.bmp", 86, 116, 1, 3, true, RGB(255, 0, 255));

	IMAGEMANAGER->addFrameImage("npc20", "img/npc/npc20/npc20.bmp", 74, 245, 1, 3, true, RGB(255, 0, 255));
	IMAGEMANAGER->addFrameImage("npc20_attack", "img/npc/npc20/npc20_attack.bmp", 109, 180, 1, 3, true, RGB(255, 0, 255));

	IMAGEMANAGER->addFrameImage("npc21", "img/npc/npc21/npc21.bmp", 152, 450, 1, 3, true, RGB(255, 0, 255));
	IMAGEMANAGER->addFrameImage("npc21_attack", "img/npc/npc21/npc21_attack.bmp", 152, 450, 1, 3, true, RGB(255, 0, 255));

	IMAGEMANAGER->addFrameImage("npc23", "img/npc/npc23/npc23.bmp", 152, 450, 1, 3, true, RGB(255, 0, 255));
	IMAGEMANAGER->addFrameImage("npc23_attack", "img/npc/npc23/npc23_attack.bmp", 152, 450, 1, 3, true, RGB(255, 0, 255));

	return S_OK;
}

void sadarimain::release(void)
{
	gameNode::release();

}
void sadarimain::update(void)
 {
	 gameNode::update();


 }
void sadarimain::render(void)
{
	//IMAGEMANAGER->findImage("bg1")->render(getMemDC(), 0, 0);

	IMAGEMANAGER->findImage("npc20")->frameRender(getMemDC(), 200, 200 , 1, 2);
 }
