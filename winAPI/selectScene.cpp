#include "StdAfx.h"
#include "selectScene.h"

selectScene::selectScene(void)
{
}

selectScene::~selectScene(void)
{
}

HRESULT selectScene::init(void)
{
	gameNode::init();

	IMAGEMANAGER->addImage("suji", "suji.bmp", WINSIZEX, WINSIZEY, true, RGB(255, 0, 255));

	_btnAngleUp = new button;
	_btnAngleUp->init("button", WINSIZEX / 2 - 200, WINSIZEY / 2, 
		PointMake(0, 1), PointMake(0, 0), cbAngleUp);

	_btnAngleDown = new button;
	_btnAngleDown->init("button", WINSIZEX / 2 + 200, WINSIZEY / 2, 
		PointMake(0, 1), PointMake(0, 0), cbAngleDown);


	return S_OK;
}

void selectScene::release(void)
{
	gameNode::release();

	_btnAngleUp->release();
	SAFE_DELETE(_btnAngleUp);

	_btnAngleDown->release();
	SAFE_DELETE(_btnAngleDown);
}

void selectScene::update(void)
{
	gameNode::update();

	_btnAngleUp->update();
	_btnAngleDown->update();
}

void selectScene::render(void)
{
	IMAGEMANAGER->findImage("suji")->render(getMemDC(), 0, 0);

	IMAGEMANAGER->findImage("suji")->alphaRender(getMemDC(), 200, 200, 200);

	IMAGEMANAGER->findImage("suji")->alphaRender(getMemDC(), 100, 100, 200, 200, 400, 400, 128);

	_btnAngleUp->render();
	_btnAngleDown->render();
}

void selectScene::cbAngleUp(void)
{
	DATABASE->setElementDataAngle("carrier",
		DATABASE->getElemnets("carrier")->angle + 0.2f);
}

void selectScene::cbAngleDown(void)
{

}
