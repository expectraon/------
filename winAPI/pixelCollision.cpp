#include "StdAfx.h"
#include "pixelCollision.h"

pixelCollision::pixelCollision(void)
{
}

pixelCollision::~pixelCollision(void)
{
}

HRESULT pixelCollision::init(void)
{
	gameNode::init();

	IMAGEMANAGER->addImage("background100", "background.bmp", 800, 800, true, RGB(255, 0, 255));

//	_testImage = IMAGEMANAGER->addImage("knight2", "knight.bmp", 612, 312, true, RGB(255, 0, 255));
//	_collision = false;



	_ball = IMAGEMANAGER->addImage("ball", "ball.bmp", 50, 50, true, RGB(255, 0, 255));
	_x = WINSIZEX / 2 - 100;
	_y = WINSIZEY / 2 + 40;

	_rc = RectMakeCenter(_x, _y, _ball->getWidth(), _ball->getHeight());

	_probeY = _y + _ball->getHeight() / 2;

	return S_OK;
}

void pixelCollision::release(void)
{
	gameNode::release();
}

void pixelCollision::update(void)
{
	gameNode::update();

	if (KEYMANAGER->isStayKeyDown(VK_RIGHT))
	{
		_x += 2.0f;
	}

	if (KEYMANAGER->isStayKeyDown(VK_LEFT))
	{
		_x -= 2.0f;
	}

	_probeY = (int)_y + _ball->getHeight() / 2;

	for (int i = _probeY - 50; i < _probeY + 50; i++)
	{
		COLORREF color = GetPixel(IMAGEMANAGER->findImage("background100")->getMemDC(), _x, i);
	
		int r = GetRValue(color);
		int g = GetGValue(color);
		int b = GetBValue(color);

		if (!(r == 255 && g == 0 && b == 255))
		{
			_y = i - _ball->getHeight() / 2;
			break;
		}
	}

	_rc = RectMakeCenter(_x, _y, _ball->getWidth(), _ball->getHeight());


	/*
	if (KEYMANAGER->isStayKeyDown(VK_LBUTTON))
	{
		COLORREF color;
		color = GetPixel(IMAGEMANAGER->findImage("knight2")->getMemDC(), _ptMouse.x, _ptMouse.y);

		int r = GetRValue(color);
		int g = GetGValue(color);
		int b = GetBValue(color);

		if (!(r == 255 && g == 0 && b == 255))
		{
			SetPixel(IMAGEMANAGER->findImage("knight2")->getMemDC(), _ptMouse.x, _ptMouse.y, RGB(255, 0, 0));
		}
	}*/
}

void pixelCollision::render(void)
{
//	_testImage->render(getMemDC(), 0, 0);

	IMAGEMANAGER->findImage("background100")->render(getMemDC(), 0, 0);

	_ball->render(getMemDC(), _rc.left, _rc.top);
}
