#include "StdAfx.h"
#include "starcraftScene.h"
#include "carrier.h"
#include "corsair.h"

starcraftScene::starcraftScene(void)
{
}

starcraftScene::~starcraftScene(void)
{
}

HRESULT starcraftScene::init(void)
{
	gameNode::init();

	_shipManager = new shipManager;
	_shipManager->init();

	/*
	_battle = new carrier;
	_battle->init("carrier", WINSIZEX / 2 - 100, WINSIZEY / 2);

	_corsair = new corsair;
	_corsair->init("corsair", WINSIZEX / 2 + 100, WINSIZEY / 2);
*/
	return S_OK;
}

void starcraftScene::release(void)
{
	gameNode::release();

	_shipManager->release();
	SAFE_DELETE(_shipManager);

	/*
	_battle->release();
	SAFE_DELETE(_battle);

	_corsair->release();
	SAFE_DELETE(_corsair);
	*/
}

void starcraftScene::update(void)
{
	gameNode::update();

	_shipManager->update();

//	_battle->update();
//	_corsair->update();
}

void starcraftScene::render(void)
{
//	IMAGEMANAGER->findImage("background")->render(getMemDC(), 0, 0);

	_shipManager->render();

//	_battle->render();
//	_corsair->render();
}

