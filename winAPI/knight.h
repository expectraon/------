#pragma once

#include "gameNode.h"
#include "jump.h"

#define SPEED 2.2f

enum KNIGHTDIRECTION
{
	KNIGHTDIRECTION_RIGHT_STOP,
	KNIGHTDIRECTION_LEFT_STOP,
	KNIGHTDIRECTION_RIGHT_RUN,
	KNIGHTDIRECTION_LEFT_RUN,
	KNIGHTDIRECTION_RIGHT_SIT,
	KNIGHTDIRECTION_LEFT_SIT,
	KNIGHTDIRECTION_RIGHT_FIRE,
	KNIGHTDIRECTION_LEFT_FIRE,
	KNIGHTDIRECTION_RIGHT_SIT_FIRE,
	KNIGHTDIRECTION_LEFT_SIT_FIRE,
	KNIGHTDIRECTION_RIGHT_JUMP,
	KNIGHTDIRECTION_LEFT_JUMP,
	KNIGHTDIRECTION_RIGHT_RUN_JUMP,
	KNIGHTDIRECTION_LEFT_RUN_JUMP
};

class knight : public gameNode
{
private:
	KNIGHTDIRECTION _knightDirection;

	jump* _jump;

	image* _image;
	animation* _knightMotion;

	float _x, _y;
	RECT _rc;

	bool _isRunning;

public:
	HRESULT init(void);
	void release(void);
	void update(void);
	void render(void);

	static void rightFire(void* obj); 
	static void leftFire(void* obj);
	static void rightSitFire(void* obj); 
	static void leftSitFire(void* obj);
	static void rightJump(void* obj);
	static void leftJump(void* obj);
	static void rightRunJump(void* obj);
	static void leftRunJump(void* obj);

	void setKnightDirection(KNIGHTDIRECTION kd) {_knightDirection = kd;}
	KNIGHTDIRECTION getKnightDirection(void) {return _knightDirection;}

	void setKnightMotion(animation* ani) {_knightMotion = ani;}

	animation* getKnightMotion(void) {return _knightMotion;}
	bool getIsRunning(void) {return _isRunning;}

	knight(void);
	virtual ~knight(void);
};
