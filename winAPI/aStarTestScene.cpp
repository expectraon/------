#include "StdAfx.h"
#include "aStarTestScene.h"

aStarTestScene::aStarTestScene(void)
{
}

aStarTestScene::~aStarTestScene(void)
{
}

HRESULT aStarTestScene::init(void)
{
	gameNode::init();

	_ast = new aStarTest;
	_ast->init();

	return 0;
}

void aStarTestScene::release(void)
{
	gameNode::release();

	SAFE_DELETE(_ast);
}

void aStarTestScene::update(void)
{
	gameNode::update();

	_ast->update();
}

void aStarTestScene::render(void)
{
	_ast->render();
}
