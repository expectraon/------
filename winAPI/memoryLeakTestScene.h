#pragma once

#include "gamenode.h"
#include "memoryLeakTest.h"

class memoryLeakTestScene : public gameNode
{
private:
	memoryLeakTest* _mlt;

public:
	virtual HRESULT init(void);
	virtual void release(void);
	virtual void update(void);
	virtual void render(void);

	memoryLeakTestScene(void);
	~memoryLeakTestScene(void);
};
