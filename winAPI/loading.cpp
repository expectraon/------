#include "StdAfx.h"
#include "loading.h"

loadItem::loadItem(void)
{

}

loadItem::~loadItem(void)
{

}

//이미지 로드 아이템
HRESULT loadItem::initForImage(string keyName, int width, int height)
{
	_kind = LOADING_KIND_IMAGE_00;

	memset(&_imageResource, 0, sizeof(tagImageResource));

	_imageResource.keyName = keyName;
	_imageResource.width = width;
	_imageResource.height = height;

	return S_OK;
}

HRESULT loadItem::initForImage(string keyName, const char* fileName, int x, int y, int width, int height, bool trans, COLORREF transColor)
{
	_kind = LOADING_KIND_IMAGE_01;

	memset(&_imageResource, 0, sizeof(tagImageResource));

	_imageResource.keyName = keyName;
	_imageResource.fileName = fileName;
	_imageResource.x = x;
	_imageResource.y = y;
	_imageResource.width = width;
	_imageResource.height = height;
	_imageResource.trans = trans;
	_imageResource.transColor = transColor;

	return S_OK;
}

HRESULT loadItem::initForImage(string keyName, const char* fileName, int width, int height, bool trans, COLORREF transColor)
{
	_kind = LOADING_KIND_IMAGE_02;

	memset(&_imageResource, 0, sizeof(tagImageResource));

	_imageResource.keyName = keyName;
	_imageResource.fileName = fileName;
	_imageResource.width = width;
	_imageResource.height = height;
	_imageResource.trans = trans;
	_imageResource.transColor = transColor;

	return S_OK;
}

HRESULT loadItem::initForFrameImage(string keyName, const char* fileName, int x, int y, int width, int height, int frameX, int frameY, bool trans, COLORREF transColor)
{
	_kind = LOADING_KIND_FRAMEIMAGE_00;

	memset(&_imageResource, 0, sizeof(tagImageResource));

	_imageResource.keyName = keyName;
	_imageResource.fileName = fileName;
	_imageResource.x = x;
	_imageResource.y = y;
	_imageResource.width = width;
	_imageResource.height = height;
	_imageResource.frameX = frameX;
	_imageResource.frameY = frameY;
	_imageResource.trans = trans;
	_imageResource.transColor = transColor;

	return S_OK;
}

HRESULT loadItem::initForFrameImage(string keyName, const char* fileName, int width, int height, int frameX, int frameY, bool trans, COLORREF transColor)
{
	_kind = LOADING_KIND_FRAMEIMAGE_01;

	memset(&_imageResource, 0, sizeof(tagImageResource));

	_imageResource.keyName = keyName;
	_imageResource.fileName = fileName;
	_imageResource.width = width;
	_imageResource.height = height;
	_imageResource.frameX = frameX;
	_imageResource.frameY = frameY;
	_imageResource.trans = trans;
	_imageResource.transColor = transColor;

	return S_OK;
}

//사운드 로드 아이템
HRESULT loadItem::initForSound(string keyName, string soundName, bool background, bool loop)
{
	_kind = LOADING_KIND_SOUND_00;

	memset(&_soundResource, 0, sizeof(tagSoundResource));

	_soundResource.keyName = keyName;
	_soundResource.soundName = soundName;
	_soundResource.background = background;
	_soundResource.loop = loop;

	return S_OK;
}

void loadItem::release(void)
{
}


///////////////////////// loading //////////////////////////////


loading::loading(void)
{
}

loading::~loading(void)
{
}

HRESULT loading::init(void)
{
	gameNode::init();

	_background = IMAGEMANAGER->addImage("loadingBackground", "loadingBackground.bmp", 1024, 768);

	_loadingBar = new progressBar;
	_loadingBar->init("loadingBarUp", "loadingBarDown", 112, 680, 800, 25);
	_loadingBar->setGauge(0, 0);

	_current = 0;	

	return S_OK;
}

void loading::release(void)
{
	gameNode::release();

	_loadingBar->release();
	SAFE_DELETE(_loadingBar);
}

void loading::update(void)
{
	gameNode::update();
	_loadingBar->update();
}

void loading::render(void)
{
	_background->render(getMemDC(), 0, 0);
	_loadingBar->render();
}

void loading::loadImage(string keyName, int width, int height)
{
	loadItem* item = new loadItem;
	item->initForImage(keyName, width, height);
	_vLoadItem.push_back(item);
}

void loading::loadImage(string keyName, const char* fileName, int x, int y, int width, int height, bool trans, COLORREF transColor)
{
	loadItem* item = new loadItem;
	item->initForImage(keyName, fileName, x, y, width, height, trans, transColor);
	_vLoadItem.push_back(item);
}

void loading::loadImage(string keyName, const char* fileName, int width, int height, bool trans, COLORREF transColor)
{
	loadItem* item = new loadItem;
	item->initForImage(keyName, fileName, width, height, trans, transColor);
	_vLoadItem.push_back(item);
}

void loading::loadFrameImage(string keyName, const char* fileName, int x, int y, int width, int height, int frameX, int frameY, bool trans, COLORREF transColor)
{
	loadItem* item = new loadItem;
	item->initForFrameImage(keyName, fileName, x, y, width, height, trans, transColor);
	_vLoadItem.push_back(item);
}

void loading::loadFrameImage(string keyName, const char* fileName, int width, int height, int frameX, int frameY, bool trans, COLORREF transColor)
{
	loadItem* item = new loadItem;
	item->initForFrameImage(keyName, fileName, width, height, frameX, frameY, trans, transColor);
	_vLoadItem.push_back(item);
}

void loading::loadSound(string keyName, const char* fileName, bool background, bool loop)
{
	loadItem* item = new loadItem;
	item->initForSound(keyName, fileName, background, loop);
	_vLoadItem.push_back(item);
}


bool loading::loadNext(void)
{
	//로딩끝~
	if (_current >= _vLoadItem.size())
	{
		_loadingBar->setGauge(_current, _vLoadItem.size());
		return false;
	}

	loadItem* item = _vLoadItem[_current];

	switch (item->getKind())
	{
		case LOADING_KIND_IMAGE_00:
		{
			tagImageResource imageResource = item->getImageResouce();
			IMAGEMANAGER->addImage(imageResource.keyName, imageResource.width, imageResource.height);
		}
		break;
		case LOADING_KIND_IMAGE_01:
		{
			tagImageResource imageResource = item->getImageResouce();
			IMAGEMANAGER->addImage(imageResource.keyName, imageResource.fileName, 
				imageResource.x, imageResource.y, imageResource.width, imageResource.height,
				imageResource.trans, imageResource.transColor);
		}
		break;
		case LOADING_KIND_IMAGE_02:
		{
			tagImageResource imageResource = item->getImageResouce();
			IMAGEMANAGER->addImage(imageResource.keyName, imageResource.fileName, 
				imageResource.width, imageResource.height,
				imageResource.trans, imageResource.transColor);
		}
		break;
		case LOADING_KIND_FRAMEIMAGE_00:
		{
			tagImageResource imageResource = item->getImageResouce();
			IMAGEMANAGER->addFrameImage(imageResource.keyName, imageResource.fileName, 
				imageResource.x, imageResource.y, imageResource.width, 
				imageResource.height, imageResource.frameX, imageResource.frameY,
				imageResource.trans, imageResource.transColor);
		}
		break;
		case LOADING_KIND_FRAMEIMAGE_01:
		{
			tagImageResource imageResource = item->getImageResouce();
			IMAGEMANAGER->addFrameImage(imageResource.keyName, imageResource.fileName, 
				imageResource.width, 
				imageResource.height, imageResource.frameX, imageResource.frameY,
				imageResource.trans, imageResource.transColor);
		}
		break;
		case LOADING_KIND_SOUND_00:
		{
			tagSoundResource soundResource = item->getSoundResouce();
			SOUNDMANAGER->addSound(soundResource.keyName, soundResource.soundName, soundResource.background, soundResource.loop);
		}
		break;
	}

	//로딩바 이미지 변경
	_loadingBar->setGauge(_current, _vLoadItem.size());

	//카운트~
	_current++;

	return true;
}


