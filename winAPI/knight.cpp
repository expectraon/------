#include "StdAfx.h"
#include "knight.h"

knight::knight(void)
{
}

knight::~knight(void)
{
}

HRESULT knight::init(void)
{
	gameNode::init();

	_knightDirection = KNIGHTDIRECTION_RIGHT_STOP;
	_isRunning = false;

	_image = IMAGEMANAGER->addFrameImage("knight", "knight.bmp", 0, 0, 612, 312, 9, 4, true, RGB(255, 0, 255));
	_x = WINSIZEX / 2 - 200;
	_y = WINSIZEY / 2 + 170;
	_rc = RectMakeCenter(_x, _y, _image->getFrameWidth(), _image->getFrameHeight());


	//키애니메이션 등록
	int arrRightStop[] = {0};
	KEYANIMANAGER->addArrayAni("knightRightStop", "knight", arrRightStop, 1, 10, false);

	int arrLeftStop[] = {9};
	KEYANIMANAGER->addArrayAni("knightLeftStop", "knight", arrLeftStop, 1, 10, false);

	int arrRightRun[] = {1, 2, 3, 4, 5, 6};
	KEYANIMANAGER->addArrayAni("knightRightRun", "knight", arrRightRun, 6, 10, true);

	int arrLeftRun[] = {10, 11, 12, 13, 14, 15};
	KEYANIMANAGER->addArrayAni("knightLeftRun", "knight", arrLeftRun, 6, 10, true);

	int arrRightSit[] = {18};
	KEYANIMANAGER->addArrayAni("knightRightSit", "knight", arrRightSit, 1, 10, false);

	int arrLeftSit[] = {27};
	KEYANIMANAGER->addArrayAni("knightLeftSit", "knight", arrLeftSit, 1, 10, false);

	int arrRightFire[] = {7, 8};
	KEYANIMANAGER->addArrayAni("knightRightFire", "knight", arrRightFire, 2, 20, false, rightFire, this);

	int arrLeftFire[] = {16, 17};
	KEYANIMANAGER->addArrayAni("knightLeftFire", "knight", arrLeftFire, 2, 20, false, leftFire, this);

	int arrRightSitFire[] = {19, 20};
	KEYANIMANAGER->addArrayAni("knightRightSitFire", "knight", arrRightSitFire, 2, 20, false, rightSitFire, this);

	int arrLeftSitFire[] = {28, 29};
	KEYANIMANAGER->addArrayAni("knightLeftSitFire", "knight", arrLeftSitFire, 2, 20, false, leftSitFire, this);

	int arrRightJump[] = {22, 24, 25};
	KEYANIMANAGER->addArrayAni("knightRightJump", "knight", arrRightJump, 3, 4, false, rightJump, this);

	int arrLeftJump[] = {31, 33, 34};
	KEYANIMANAGER->addArrayAni("knightLeftJump", "knight", arrLeftJump, 3, 4, false, leftJump, this);

	int arrRightRunJump[] = {21, 23};
	KEYANIMANAGER->addArrayAni("knightRightRunJump", "knight", arrRightRunJump, 2, 3, false, rightRunJump, this);

	int arrLeftRunJump[] = {30, 32};
	KEYANIMANAGER->addArrayAni("knightLeftRunJump", "knight", arrLeftRunJump, 2, 3, false, leftRunJump, this);
	

	_knightMotion = KEYANIMANAGER->findAnimation("knightRightStop");

	_jump = new jump;
	_jump->init();

	return S_OK;
}

void knight::release(void)
{
	gameNode::release();

	_jump->release();
	SAFE_DELETE(_jump);
}

void knight::update(void)
{
	//오른쪽 뛰자
	if (KEYMANAGER->isOnceKeyDown('D'))
	{
		_knightDirection = KNIGHTDIRECTION_RIGHT_RUN;
		_knightMotion = KEYANIMANAGER->findAnimation("knightRightRun");
		_knightMotion->start();
	}
	else if (KEYMANAGER->isOnceKeyUp('D'))
	{
		_knightDirection = KNIGHTDIRECTION_RIGHT_STOP;
		_knightMotion = KEYANIMANAGER->findAnimation("knightRightStop");
		_knightMotion->start();
	}

	//왼쪽 뛰자
	if (KEYMANAGER->isOnceKeyDown('A'))
	{
		_knightDirection = KNIGHTDIRECTION_LEFT_RUN;
		_knightMotion = KEYANIMANAGER->findAnimation("knightLeftRun");
		_knightMotion->start();
	}
	else if (KEYMANAGER->isOnceKeyUp('A'))
	{
		_knightDirection = KNIGHTDIRECTION_LEFT_STOP;
		_knightMotion = KEYANIMANAGER->findAnimation("knightLeftStop");
		_knightMotion->start();
	}

	//앉는다
	if (KEYMANAGER->isOnceKeyDown('S'))
	{
		if (_knightDirection == KNIGHTDIRECTION_RIGHT_STOP || _knightDirection == KNIGHTDIRECTION_RIGHT_RUN)
		{
			_knightDirection = KNIGHTDIRECTION_RIGHT_SIT;
			_knightMotion = KEYANIMANAGER->findAnimation("knightRightSit");
			_knightMotion->start();
		}
		else if (_knightDirection == KNIGHTDIRECTION_LEFT_STOP || _knightDirection == KNIGHTDIRECTION_LEFT_RUN)
		{
			_knightDirection = KNIGHTDIRECTION_LEFT_SIT;
			_knightMotion = KEYANIMANAGER->findAnimation("knightLeftSit");
			_knightMotion->start();
		}
	}
	else if (KEYMANAGER->isOnceKeyUp('S'))
	{
		if (_knightDirection == KNIGHTDIRECTION_RIGHT_SIT)
		{
			_knightDirection = KNIGHTDIRECTION_RIGHT_STOP;
			_knightMotion = KEYANIMANAGER->findAnimation("knightRightStop");
			_knightMotion->start();
		}
		else if (_knightDirection == KNIGHTDIRECTION_LEFT_SIT)
		{
			_knightDirection = KNIGHTDIRECTION_LEFT_STOP;
			_knightMotion = KEYANIMANAGER->findAnimation("knightLeftStop");
			_knightMotion->start();
		}
	}

	//창던지기
	if (KEYMANAGER->isOnceKeyDown('H'))
	{
		//서서 창던지기
		if (_knightDirection == KNIGHTDIRECTION_RIGHT_RUN || _knightDirection == KNIGHTDIRECTION_RIGHT_STOP)
		{
			if (_knightDirection == KNIGHTDIRECTION_RIGHT_RUN) _isRunning = true;
			else _isRunning = false;
			_knightDirection = KNIGHTDIRECTION_RIGHT_FIRE;
			_knightMotion = KEYANIMANAGER->findAnimation("knightRightFire");
			_knightMotion->start();
		}

		else if (_knightDirection == KNIGHTDIRECTION_LEFT_RUN || _knightDirection == KNIGHTDIRECTION_LEFT_STOP)
		{
			if (_knightDirection == KNIGHTDIRECTION_LEFT_RUN) _isRunning = true;
			else _isRunning = false;
			_knightDirection = KNIGHTDIRECTION_LEFT_FIRE;
			_knightMotion = KEYANIMANAGER->findAnimation("knightLeftFire");
			_knightMotion->start();
		}

		//앉아서 창 던지기
		if (_knightDirection == KNIGHTDIRECTION_RIGHT_SIT)
		{
			_knightDirection = KNIGHTDIRECTION_RIGHT_SIT_FIRE;
			_knightMotion = KEYANIMANAGER->findAnimation("knightRightSitFire");
			_knightMotion->start();
		}
		else if (_knightDirection == KNIGHTDIRECTION_LEFT_SIT)
		{
			_knightDirection = KNIGHTDIRECTION_LEFT_SIT_FIRE;
			_knightMotion = KEYANIMANAGER->findAnimation("knightLeftSitFire");
			_knightMotion->start();
		}
	}

	if (KEYMANAGER->isOnceKeyDown('J'))
	{
		_jump->jumping(&_x, &_y, 8.2f, 0.4f);

		if (_knightDirection == KNIGHTDIRECTION_RIGHT_STOP)
		{
			_knightDirection = KNIGHTDIRECTION_RIGHT_JUMP;
			_knightMotion = KEYANIMANAGER->findAnimation("knightRightJump");
			_knightMotion->start();
		}
		else if (_knightDirection == KNIGHTDIRECTION_LEFT_STOP)
		{
			_knightDirection = KNIGHTDIRECTION_LEFT_JUMP;
			_knightMotion = KEYANIMANAGER->findAnimation("knightLeftJump");
			_knightMotion->start();
		}
		else if (_knightDirection == KNIGHTDIRECTION_RIGHT_RUN)
		{
			_knightDirection = KNIGHTDIRECTION_RIGHT_RUN_JUMP;
			_knightMotion = KEYANIMANAGER->findAnimation("knightRightRunJump");
			_knightMotion->start();
		}
		else if (_knightDirection == KNIGHTDIRECTION_LEFT_RUN)
		{
			_knightDirection = KNIGHTDIRECTION_LEFT_RUN_JUMP;
			_knightMotion = KEYANIMANAGER->findAnimation("knightLeftRunJump");
			_knightMotion->start();
		}
	}


	_rc = RectMakeCenter(_x, _y, _image->getFrameWidth(), _image->getFrameHeight());

	switch (_knightDirection)
	{
		case KNIGHTDIRECTION_LEFT_STOP: case KNIGHTDIRECTION_RIGHT_STOP:
		break;
		case KNIGHTDIRECTION_RIGHT_RUN: case KNIGHTDIRECTION_RIGHT_RUN_JUMP:
			_x += SPEED;
		break;
		case KNIGHTDIRECTION_LEFT_RUN: case KNIGHTDIRECTION_LEFT_RUN_JUMP:
			_x -= SPEED;
		break;

	}

	KEYANIMANAGER->update();

	_jump->update();

	gameNode::update();
}

void knight::render(void)
{
	_image->aniRender(getMemDC(), _rc.left, _rc.top, _knightMotion);
}

//오른쪽으로 창 던지기
void knight::rightFire(void* obj)
{
	knight* k = (knight*)obj;

	if (k->getIsRunning())
	{
		k->setKnightDirection(KNIGHTDIRECTION_RIGHT_RUN);
		k->setKnightMotion(KEYANIMANAGER->findAnimation("knightRightRun"));
		k->getKnightMotion()->start();
		return;
	}

	k->setKnightDirection(KNIGHTDIRECTION_RIGHT_STOP);
	k->setKnightMotion(KEYANIMANAGER->findAnimation("knightRightStop"));
	k->getKnightMotion()->start();
}

//왼쪽으로 창 던지기
void knight::leftFire(void* obj)
{
	knight* k = (knight*)obj;

	if (k->getIsRunning())
	{
		k->setKnightDirection(KNIGHTDIRECTION_LEFT_RUN);
		k->setKnightMotion(KEYANIMANAGER->findAnimation("knightLeftRun"));
		k->getKnightMotion()->start();
		return;
	}

	k->setKnightDirection(KNIGHTDIRECTION_LEFT_STOP);
	k->setKnightMotion(KEYANIMANAGER->findAnimation("knightLeftStop"));
	k->getKnightMotion()->start();
}

//오른쪽으로 앉아서 창 던지기
void knight::rightSitFire(void* obj)
{
	knight* k = (knight*)obj;

	k->setKnightDirection(KNIGHTDIRECTION_RIGHT_SIT);
	k->setKnightMotion(KEYANIMANAGER->findAnimation("knightRightSit"));
	k->getKnightMotion()->start();
}

//왼쪽으로 앉아서 창 던지기
void knight::leftSitFire(void* obj)
{
	knight* k = (knight*)obj;

	k->setKnightDirection(KNIGHTDIRECTION_LEFT_SIT);
	k->setKnightMotion(KEYANIMANAGER->findAnimation("knightLeftSit"));
	k->getKnightMotion()->start();
}

void knight::rightJump(void* obj)
{
	knight* k = (knight*)obj;

	k->setKnightDirection(KNIGHTDIRECTION_RIGHT_STOP);
	k->setKnightMotion(KEYANIMANAGER->findAnimation("knightRightStop"));
	k->getKnightMotion()->start();
}

void knight::leftJump(void* obj)
{
	knight* k = (knight*)obj;

	k->setKnightDirection(KNIGHTDIRECTION_LEFT_STOP);
	k->setKnightMotion(KEYANIMANAGER->findAnimation("knightLeftStop"));
	k->getKnightMotion()->start();
}

void knight::rightRunJump(void* obj)
{
	knight* k = (knight*)obj;

	k->setKnightDirection(KNIGHTDIRECTION_RIGHT_RUN);
	k->setKnightMotion(KEYANIMANAGER->findAnimation("knightRightRun"));
	k->getKnightMotion()->start();
}

void knight::leftRunJump(void* obj)
{
	knight* k = (knight*)obj;

	k->setKnightDirection(KNIGHTDIRECTION_LEFT_RUN);
	k->setKnightMotion(KEYANIMANAGER->findAnimation("knightLeftRun"));
	k->getKnightMotion()->start();
}
