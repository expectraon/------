#pragma once

#include "gamenode.h"
#include "knight.h"

class ghotsnGoblinsScene : public gameNode
{
private:
	knight* _knight;

public:
	virtual HRESULT init(void);
	virtual void release(void);
	virtual void update(void);
	virtual void render(void);

	ghotsnGoblinsScene(void);
	~ghotsnGoblinsScene(void);
};
