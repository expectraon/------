#include "StdAfx.h"
#include "enemyManager.h"
#include "player.h"

enemyManager::enemyManager(void)
{
}

enemyManager::~enemyManager(void)
{
}

HRESULT enemyManager::init(void)
{
	gameNode::init();

	IMAGEMANAGER->addFrameImage("jellyFish", "jellyFish.bmp", 0, 0, 1140, 47, 19, 1, true, RGB(255, 0, 255));

	IMAGEMANAGER->addImage("bullet", "bullet.bmp", 12, 12, true, RGB(255, 0, 255));

	//총알 초기화
	_bullet = new bullet;
	_bullet->init("bullet", 15, 600);

	return S_OK;
}

void enemyManager::release(void)
{
	//나오면서 쫄따구 다 날려준다
	for (_viMinion = _vMinion.begin(); _viMinion != _vMinion.end(); ++_viMinion)
	{
		(*_viMinion)->release();
		SAFE_DELETE((*_viMinion));
	}

	//쫄따구들이 머물렀던 방도 완전히 날린다
	_vMinion.clear();

	_bullet->release();
	SAFE_DELETE(_bullet);

	gameNode::release();
}

//업데이트
void enemyManager::update(void)
{
	gameNode::update();

	//포문 (혹은 반복문) : 쫄따구가 들어있는 벡터를 돌려서 업데이트 함수 실행해 준다
	for (_viMinion = _vMinion.begin(); _viMinion != _vMinion.end(); ++_viMinion)
	{
		(*_viMinion)->update();
	}
	
	//쫄따구 총쏜다
	minionBulletFire();

	//총알업데이트
	_bullet->update();
}

//렌더
void enemyManager::render(void)
{
	//쫄따구가 들어 있는 벡터를 돌려서 렌더 함수 실행해 준다
	for (_viMinion = _vMinion.begin(); _viMinion != _vMinion.end(); ++_viMinion)
	{
		(*_viMinion)->render();
	}

	//총알 렌더
	_bullet->render();
}

//쫄따구 셋업
void enemyManager::setMinion(void)
{
	for (int i = 0; i < 3; i++)
	{
		for (int j = 0; j < 5; j++)
		{
			//enemy*로 선언 후 초기화는 minion으로 해준다
			//virtual의 기능을 사용하기 위함이다.
			enemy* jellyFish;
			jellyFish = new minion;
			jellyFish->init("jellyFish", PointMake(100 + j * 100, 100 + i * 100));
			_vMinion.push_back(jellyFish); //생성 후 쫄따구 벡터로 푸쉬~~
		}
	}
}

//쫄따구 뒤짐
void enemyManager::removeMinion(int arrNum)
{
	SAFE_DELETE(_vMinion[arrNum]);
	_vMinion.erase(_vMinion.begin() + arrNum);
}

void enemyManager::minionBulletFire(void)
{
	for (_viMinion = _vMinion.begin(); _viMinion != _vMinion.end(); ++_viMinion)
	{
		if ((*_viMinion)->bulletCountFire())
		{
			RECT rc = (*_viMinion)->getRect();
			_bullet->fire(rc.left + (rc.right - rc.left) / 2, 
				rc.top + 15, getAngle(rc.left + (rc.right - rc.left) / 2,
				rc.bottom + (rc.top - rc.bottom) / 2, 
				_player->getImage()->getCenterX(), _player->getImage()->getCenterY()),
				RND->getFromFloatTo(0.5f, 3.0f));

		}
	}
}