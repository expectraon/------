#pragma once

#include "gameNode.h"
#include "minion.h"
#include <vector>
#include "bullets.h"

//�÷��̾� Ŭ���� ���漱��
class player;

class enemyManager : public gameNode
{
private:
	//���ʹ� ���� �ڷ��� ����
	typedef vector<enemy*> VEC_ENEMY;
	typedef vector<enemy*>::iterator VECITER_ENEMY;

private:
	//���� ���ʹ� ���� ����
	VEC_ENEMY _vMinion; //�̵��� ����
	VECITER_ENEMY _viMinion; //�̵��� ���ͷ�����(�ݺ���)

	player* _player;
	bullet* _bullet;

public:
	HRESULT init(void);
	void release(void);
	void update(void);
	void render(void);

	//�̵��� �¾�
	void setMinion(void);
	
	//�̵��� �Ѿ� ��..
	void minionBulletFire(void);

	//�̵��� ����
	void removeMinion(int arrNum);

	vector<enemy*> getVMinion(void) {return _vMinion;}
	vector<enemy*>::iterator getVIMinion(void) {return _viMinion;}

	bullet* getBullet(void) {return _bullet;}

	void setPlayerMemoryAddressLink(player* pa) {_player = pa;}

	enemyManager(void);
	~enemyManager(void);
};
