#pragma once
#include "gamenode.h"
#include "actionTest.h"

class actionTestScene : public gameNode
{
private:
	actionTest* _actionTest;

public:
	virtual HRESULT init(void);
	virtual void release(void);
	virtual void update(void);
	virtual void render(void);

	actionTestScene(void);
	~actionTestScene(void);
};
