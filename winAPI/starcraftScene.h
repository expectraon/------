#pragma once
#include "gamenode.h"
#include "shipManager.h"

class starcraftScene : public gameNode
{
private:
//	ship* _battle;
//	ship* _corsair;

	shipManager* _shipManager;

public:
	virtual HRESULT init(void);
	virtual void release(void);
	virtual void update(void);
	virtual void render(void);

	starcraftScene(void);
	virtual ~starcraftScene(void);
};
