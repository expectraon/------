#include "StdAfx.h"
#include "memoryLeakTestScene.h"

memoryLeakTestScene::memoryLeakTestScene(void)
{
}

memoryLeakTestScene::~memoryLeakTestScene(void)
{
}

HRESULT memoryLeakTestScene::init(void)
{
	gameNode::init();

	_mlt = new memoryLeakTest;
	_mlt->init();

	return S_OK;
}

void memoryLeakTestScene::release(void)
{
	gameNode::release();

	SAFE_DELETE(_mlt);
}

void memoryLeakTestScene::update(void)
{
	gameNode::update();
}

void memoryLeakTestScene::render(void)
{

}
