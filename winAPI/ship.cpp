#include "StdAfx.h"
#include "ship.h"

ship::ship(void)
{
}

ship::~ship(void)
{
}

HRESULT ship::init(const char* imageName, int x, int y)
{
	gameNode::init();

	//배 초기화
	_image = IMAGEMANAGER->findImage(imageName);
	_x = x;
	_y = y;
	_angle = DATABASE->getElemnets("carrier")->angle;
	_rc = RectMake(_x, _y, 
		_image->getFrameWidth(), _image->getFrameHeight());
	_speed = 0.0f;
	_radius = _image->getFrameWidth() / 2;

	return S_OK;
}

void ship::release(void)
{
	gameNode::release();
}

void ship::update(void)
{
	gameNode::update();

//	keyControl();
	move();
}

void ship::render(void)
{
//	draw();
}

//쉽 관련 가상화 함수
void ship::draw(void)
{
	_image->frameRender(getMemDC(), _rc.left, _rc.top);

	char str[128];

	sprintf(str, "%d도, %.2f", int(_angle * 180 / PI), _speed);
	TextOut(getMemDC(), _rc.left, _rc.top, str, strlen(str));
}

//포커스 드로우
void ship::focusDraw(void)
{
	char str[128];

	//위치
	sprintf(str, "좌표 x : %d, y : %d", _rc.left, _rc.top);
	TextOut(getMemDC(), 500, 10, str, strlen(str));

	//각도
	sprintf(str, "각도 : %d", int(_angle * 180.0f / PI));
	TextOut(getMemDC(), 500, 30, str, strlen(str));

	//스피드
	sprintf(str, "속도 : %.2f", _speed);
	TextOut(getMemDC(), 500, 50, str, strlen(str));

	_image->frameRender(getMemDC(), WINSIZEX / 2 - _image->getFrameWidth() / 2,
		WINSIZEY / 2 - _image->getFrameHeight() / 2);
}

//디폴트 드로우
void ship::defaultDraw(RECT rcFocus)
{
	RECT rcSour;
	RECT rcTemp;
	int x, y;

	rcSour.left = rcFocus.left - ((WINSIZEX / 2) - (rcFocus.right - rcFocus.left) / 2);
	rcSour.top = rcFocus.top - ((WINSIZEY / 2) - (rcFocus.bottom - rcFocus.top) / 2);
	rcSour.right = rcSour.left + WINSIZEX;
	rcSour.bottom = rcSour.top + WINSIZEY;

	if (!(IntersectRect(&rcTemp, &_rc, &rcSour))) return;

	x = _rc.left - rcSour.left;
	y = _rc.top - rcSour.top;

	_image->frameRender(getMemDC(), x, y);
}

void ship::keyControl(void)
{
}

void ship::move(void)
{
	//프레임 변경
	int frame;
	float angle;

	angle = _angle + PI_32;
	if (angle >= PI * 2) angle -= PI2;

	frame = int(angle / PI_16);
	_image->setFrameX(frame);

	float elpasedTime = TIMEMANAGER->getElapsedTime();
	float moveSpeed = elpasedTime * 200;

	_x += cosf(_angle) * _speed * moveSpeed;
	_y += -sinf(_angle) * _speed * moveSpeed;

	_rc = RectMakeCenter(_x, _y, 
		_image->getFrameWidth(), _image->getFrameHeight());
}

