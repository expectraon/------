#include "StdAfx.h"
#include "enemy.h"

enemy::enemy(void)
{
}

enemy::~enemy(void)
{
}

HRESULT enemy::init(const char* imageName, POINT position)
{
	gameNode::init();

	_currentFrameX = _currentFrameY = 0;
	_count = 0;

	//쫄따구 전기액션의 시간설정을 랜덤으로 설정.
	_rndTimeCount = RND->getFromIntTo(3, 6);

	//메모리에 있는 이미지 찾아서 임시 방에 넣어준다 _image는 파인드이미지로 찾은 이미지를 가리키고 있다.
	_image = IMAGEMANAGER->findImage(imageName);
	//렉트 설정
	_rc = RectMakeCenter(position.x, position.y, _image->getFrameWidth(),
		_image->getFrameHeight());

	_bulletFireCount = 0;
	_rndFireCount = RND->getFromIntTo(50, 100);

	return S_OK;
}

void enemy::release(void)
{
	gameNode::release();
}

void enemy::update(void)
{
	gameNode::update();

	animation();
	move();
}

void enemy::render(void)
{
	draw();
}

//움직이는 부분은 상속받은 자식클래스에서 재정의 해 구현
void enemy::move(void)
{
}

void enemy::draw(void)
{
	//프레임 애니메이션 렌더
	_image->frameRender(getMemDC(), _rc.left, _rc.top, _currentFrameX, _currentFrameY);
}

void enemy::animation(void)
{
	_count++;
	if (_count % _rndTimeCount == 0)
	{
		if (_currentFrameX >= _image->getMaxFrameX())
		{
			_currentFrameX = 0;
		}

		_currentFrameX++;
		_count = 0;
	}
}

bool enemy::bulletCountFire(void)
{
	_bulletFireCount++;
	if (_bulletFireCount % _rndFireCount == 0)
	{
		_rndFireCount = RND->getFromIntTo(50, 100);
		_bulletFireCount = 0;
		return true;
	}

	return false;
}
