#pragma once

#include "gamenode.h"
#include "tile.h"
#include <vector>
#include <string>

#define TILEWIDTH 32
#define TILEHEIGHT 32
#define TILENUMX ((int)(WINSIZEX / TILEWIDTH))
#define TILENUMY ((int)(WINSIZEY / TILEHEIGHT))

class aStarTest : public gameNode
{
private:
	vector<tile*> _vTotalList;
	vector<tile*>::iterator _viTotalList;
	vector<tile*> _vOpenList;
	vector<tile*>::iterator _viOpenList;
	vector<tile*> _vCloseList;
	vector<tile*>::iterator _viCloseList;

	tile* _startTile;
	tile* _endTile;
	tile* _currentTile;

public:
	virtual HRESULT init(void);
	virtual void release(void);
	virtual void update(void);
	virtual void render(void);

	void setTiles(void);

	void pathFinder(tile* currentTile);
	vector<tile*> addOpenList(tile* currentTile);

	aStarTest(void);
	virtual ~aStarTest(void);
};
