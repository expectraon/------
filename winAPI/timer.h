#pragma once

class timer
{
private:
	bool		_isHardWare;		//고성능 타이머 지원되냐?
	float		_timeScale;			//시간 경과량 조절	
	float		_timeElapsed;		//마지막 시간과 현재시간의 경과량

	__int64		_curTime;			//현재시간
	__int64		_lastTime;			//마지막 시간
	__int64		_periodFrequency;	//시간주기

	unsigned long _frameRate;		//FPS
	unsigned long _FPSFrameCount;	//FPS 카운트
	float _FPSTimeElapsed;			//FPS 경과량
	float _worldTime;				//기준시간

public:
	//초기화
	HRESULT init(void);

	//현재시간 계산
	void tick(float lockFPS = 0.0f);

	//현재 FPS
	unsigned long getFrameRate(char* str = NULL) const;

	//마지막 시간과 현재 시간의 경과량
	inline float getElapsedTime(void) const {return _timeElapsed;}

	//전체 시간 경과
	inline float getWorldTime(void) const {return _worldTime;}

	timer(void) {};
	~timer(void) {};
};
