#include "stdafx.h"
#include "frameAnimation.h"


frameAnimation::frameAnimation()
{
}


frameAnimation::~frameAnimation()
{
}

HRESULT frameAnimation::init(void)
{
	gameNode::init();
	
	_ad = ANIMATIONDIRECTION_NULL;
	_worldTimeCount = TIMEMANAGER->getWorldTime();
	_isPlay = false;
	_rewind = false;

	return S_OK;
}

void frameAnimation::release(void)
{
	gameNode::release();
}

void frameAnimation::update(void)
{
	gameNode::update();

	if (!_isPlay) return;

	switch (_ad)
	{
		case ANIMATIONDIRECTION_X:
			if (_worldTimeCount + _count <= TIMEMANAGER->getWorldTime())
			{
				_worldTimeCount = TIMEMANAGER->getWorldTime();

				_xFrameCount++;
				_image->setFrameX(_xFrameCount);

				if (_xFrameCount > _xFrame)
				{
					if (_rewind) _image->setFrameX(0);
					else _image->setFrameX(_xFrameCount - 1);
					_isPlay = false;
				}
			}
		break;
		case ANIMATIONDIRECTION_Y:
			if (_worldTimeCount + _count <= TIMEMANAGER->getWorldTime())
			{
				_worldTimeCount = TIMEMANAGER->getWorldTime();

				_yFrameCount++;
				_image->setFrameY(_yFrameCount);

				if (_yFrameCount > _yFrame)
				{
					if (_rewind) _image->setFrameY(0);
					else _image->setFrameY(_yFrameCount - 1);
					_isPlay = false;
				}
			}
		break;
	}
}

void frameAnimation::render(void)
{
}

void frameAnimation::animationXFrame(image* img, int xFrame, float count, bool rewind)
{
	if (_isPlay)return;
	_isPlay = true;
	_count = count;
	_ad = ANIMATIONDIRECTION_X;
	_worldTimeCount = TIMEMANAGER->getWorldTime();
	_image = img;
	_xFrame = xFrame;
	_yFrame = 0;
	_xFrameCount = 0;
	_yFrameCount = 0;
	_rewind = rewind;
}

void frameAnimation::animationYFrame(image* img, int yFrame, float count, bool rewind)
{
	if (_isPlay)return;
	_isPlay = true;
	_count = count;
	_ad = ANIMATIONDIRECTION_Y;
	_worldTimeCount = TIMEMANAGER->getWorldTime();
	_image = img;
	_xFrame = 0;
	_yFrame = yFrame;
	_xFrameCount = 0;
	_yFrameCount = 0;
	_rewind = rewind;
}