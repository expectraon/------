#pragma once

#include "singletonBase.h"
#include <map>

class image;
class animation;

class keyAnimationManager : public singletonBase <keyAnimationManager>
{
private:
	typedef map<string, animation*> arrAnimation;
	typedef map<string, animation*>::iterator iterAnimation;
	
private:
	arrAnimation _mTotalAnimation;

public:
	HRESULT init(void);
	void release(void);
	void update(void);
	void render(void);

	void addDefaultAni(string keyName, char* imageName, int fps, bool reverse, bool loop);
	void addDefaultAni(string keyName, char* imageName, int fps, bool reverse, bool loop, void* cbFunction);
	void addDefaultAni(string keyName, char* imageName, int fps, bool reverse, bool loop, void* cbFunction, void* obj);

	void addArrayAni(string keyName, char* imageName, int* arr, int arrLen, int fps, bool loop);
	void addArrayAni(string keyName, char* imageName, int* arr, int arrLen, int fps, bool loop, void* cbFunction);
	void addArrayAni(string keyName, char* imageName, int* arr, int arrLen, int fps, bool loop, void* cbFunction, void* obj);
	
	void addCoordinateAni(string keyName, char* imageName, int start, int end, int fps, bool reverse, bool loop);
	void addCoordinateAni(string keyName, char* imageName, int start, int end, int fps, bool reverse, bool loop, void* cbFunction);
	void addCoordinateAni(string keyName, char* imageName, int start, int end, int fps, bool reverse, bool loop, void* cbFunction, void* obj);

	void start(string keyName);
	void stop(string keyName);
	void pause(string keyName);
	void resume(string keyName);

	//애니메이션 찾기
	animation* findAnimation(string keyName);

	//애니메이션 지운다
	void deleteAll(void);

	keyAnimationManager(void);
	~keyAnimationManager(void);
};
