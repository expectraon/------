#include "StdAfx.h"
#include "ghotsnGoblinsScene.h"

ghotsnGoblinsScene::ghotsnGoblinsScene(void)
{
}

ghotsnGoblinsScene::~ghotsnGoblinsScene(void)
{
}

HRESULT ghotsnGoblinsScene::init(void)
{
	gameNode::init();

	IMAGEMANAGER->addImage("ggsBackground", "ggsBackground.bmp", 800, 600);


	_knight = new knight;
	_knight->init();

	return S_OK;
}

void ghotsnGoblinsScene::release(void)
{
	gameNode::release();

	_knight->release();
	SAFE_DELETE(_knight);
}

void ghotsnGoblinsScene::update(void)
{
	gameNode::update();

	_knight->update();
}

void ghotsnGoblinsScene::render(void)
{
	IMAGEMANAGER->findImage("ggsBackground")->render(getMemDC(), 0, 0);

	_knight->render();
}
