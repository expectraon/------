#pragma once

#include "gamenode.h"
#include <crtdbg.h> //메모리 할당과 관련된 부분을 확인하기 위해서 crtdbg헤더를 인클루드 한당

//메모리 빵꾸 찾자~~
class memoryLeakTest : public gameNode
{
public:
	virtual HRESULT init(void);
	virtual void release(void);
	virtual void update(void);
	virtual void render(void);

	memoryLeakTest(void);
	~memoryLeakTest(void);
};
