#include "stdafx.h"
#include "gameStudy.h"

//==============================================
//20140603 ## 전역변수 써준다~ ##
//==============================================
HINSTANCE	_hInstance;					//프로그램 인스턴스 (메모리상에 할당되어 실행중인 프로그램)
HWND		_hWnd;						//윈도우 핸들
POINT		_ptMouse = {0, 0};			//마우스 위치
BOOL		_leftButtonDown = false;	//왼쪽 마우스버튼 눌렀냐
gameStudy	_gs;						//게임스터디

//==============================================
//20140603 ## 함수의 프로토타입 써준다~ ##
//==============================================
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
void setWindowSize(int x, int y, int width, int height);

//LRESULT		1, -1, 0이나 비트 플래그의 반환값을 가진다
//hInstance		프로그램 인스턴스 핸들
//hPrevInstance 이전에 실행된 인스턴스 핸들
//lpszCmdParam	명령행으로 입력된 프로그램 인수
//cmdShow		프로그램이 시작될 형태 (최소화, 보통등등...)
int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdParam, int cmdShow)
{
	MSG message;				//운영체제에서 발생하는 메시지 정보를 저장하기 위한 구조체
	WNDCLASS wndClass;			//윈도우의 정보를 저장하기 위한 구조체
	_hInstance = hInstance;		//메시지 처리함수 지정. 메시지 발생하면 지정함수 호출해서 메시지 처리한다.
	wndClass.cbClsExtra = 0;	//클래스 여분 메모리 (MDI멀티 다중 뷰 구조만 사용한다)
	wndClass.cbWndExtra = 0;	//윈도우 여분 메모리
	wndClass.hbrBackground = (HBRUSH)GetStockObject(WHITE_BRUSH);	//백그라운드
	wndClass.hCursor = LoadCursor(NULL, IDC_ARROW);					//커서
	wndClass.hIcon = LoadIcon(NULL, IDI_APPLICATION);				//아이콘
	wndClass.hInstance = hInstance;									//윈도우 인스턴스
	wndClass.lpfnWndProc = (WNDPROC)WndProc;						//프로시져 함수(메시지 처리함수)
	wndClass.lpszClassName = WINNAME;								//클래스 이름
	wndClass.lpszMenuName = NULL;									//메뉴이름
	wndClass.style = CS_HREDRAW | CS_VREDRAW;						//윈도우 스타일

	//윈도우 클래스 등록
	RegisterClass(&wndClass);

#ifdef FULLSCREEN

	DEVMODE dm;

	ZeroMemory(&dm, sizeof(DEVMODE));
	dm.dmSize = sizeof(DEVMODE);
	dm.dmDisplayFlags = 0;
	dm.dmBitsPerPel = 32;
	dm.dmPelsWidth = 800;
	dm.dmPelsHeight = 600;
	dm.dmFields = DM_BITSPERPEL | DM_PELSWIDTH | DM_PELSHEIGHT | DM_DISPLAYFREQUENCY;

	//원래 화면 해상도로 되돌아 가는 코드..
	if (ChangeDisplaySettings(&dm, CDS_FULLSCREEN) != DISP_CHANGE_SUCCESSFUL)
	{
		ChangeDisplaySettings(&dm, 0);
	}

	_hWnd = CreateWindow(
		WINNAME,				//윈도우 클래스 이름
		WINNAME,				//윈도우 타이틀바에 나타날 이름
		WS_POPUPWINDOW | WS_MAXIMIZE,				//윈도우(창) 스타일
		0,				//윈도우 화면 좌표 x
		0,				//윈도우 화면 좌표 y
		GetSystemMetrics(SM_CXSCREEN),				//윈도우 화면 좌표 width
		GetSystemMetrics(SM_CYSCREEN),				//윈도우 화면 좌표 height
		NULL,					//부모 윈도우
		(HMENU)NULL,			//메뉴핸들
		hInstance,				//인스턴스 윈도우 지정
		NULL);					//mdl클라이언트 윈도우 및 자식 윈도우를
	//생성하면 지정해주되 사용하지 않을 경우 NULL~~~~

		setWindowSize(WINSTARTX, WINSTARTY, 
			GetSystemMetrics(SM_CXSCREEN), GetSystemMetrics(SM_CYSCREEN));
#else
	//윈도우 생성
	_hWnd = CreateWindow(
		WINNAME,				//윈도우 클래스 이름
		WINNAME,				//윈도우 타이틀바에 나타날 이름
		WINSTYLE,				//윈도우(창) 스타일
		WINSTARTX,				//윈도우 화면 좌표 x
		WINSTARTY,				//윈도우 화면 좌표 y
		WINSIZEX,				//윈도우 화면 좌표 width
		WINSIZEY,				//윈도우 화면 좌표 height
		NULL,					//부모 윈도우
		(HMENU)NULL,			//메뉴핸들
		hInstance,				//인스턴스 윈도우 지정
		NULL);					//mdl클라이언트 윈도우 및 자식 윈도우를
	//생성하면 지정해주되 사용하지 않을 경우 NULL~~~~~

	setWindowSize(WINSTARTX, WINSTARTY, WINSIZEX, WINSIZEY);
#endif

	//화면에 윈도우 보여준다
	ShowWindow(_hWnd, cmdShow);

//	_CrtSetDbgFlag(_CRTDBG_ALLOC_MEM_DF | _CRTDBG_LEAK_CHECK_DF);

//	_CrtSetBreakAlloc(182);

	//노드 초기화
	if (FAILED(_gs.init()))
	{
		return 0;	//실패시에는 바로 종료한다
	}

	//게임용
	while (true)
	{
		//메시지 큐에 메시지가 있으면 메시지 처리~
		//PeekMessage : 메시지가 없더라도 리턴되는 함수로, 논블럭 되는 함수이다.
		if (PeekMessage(&message, NULL, 0, 0, PM_REMOVE))
		{
			if (message.message == WM_QUIT) break;
			TranslateMessage(&message);
			DispatchMessage(&message);
		}
		else
		{
			TIMEMANAGER->update(60.0f); //초당 60으로 고정
			//처리할 메시지 없으면 렌더 함수 호출
			_gs.update();
			_gs.render();
		}
	}
	
	/*
	//메시지 루프
	//GetMessage : 메시지 큐에 입력된 메시지를 읽어서 메시지 구조체에 저장한다
	while (GetMessage(&message, 0, 0, 0)) //일반 프로그램용
	{
		//TranslateMessage : 키보드 입력메시지 처리를 담당한다
		//입력된 키가 문자키인지 확인후에 대문자 혹은 소문자, 한글, 영문인지에
		//대한 WM_CHAR메시지를 발생시킴
		
		//DispatchMessage : 윈도우 프로시져에서 전달된 메시지를 실제 윈도우로
		//전달해 주는 기능을 한다.
		TranslateMessage(&message);
		DispatchMessage(&message);
		
		//메시지 전달후에 메시지 루프의 제일 앞으로 돌아가 다음 메시지를 기다린다
	}
*/
	//게임스터디 해제
	_gs.release();

//	_CrtDumpMemoryLeaks();

	//윈도우 클래스 등록 해제해 준다
	UnregisterClass(WINNAME, hInstance);

	return message.wParam;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT iMessage, WPARAM wParam, LPARAM lParam)
{
	return _gs.MainProc(hWnd, iMessage, wParam, lParam);
}

//작업영역 사이즈 보정 함수
void setWindowSize(int x, int y, int width, int height)
{
	RECT rc;
	rc.left = 0;
	rc.top = 0;
	rc.right = width;
	rc.bottom = height;

	AdjustWindowRect(&rc, WINSTYLE, false); //false는 메뉴 있는지 없는지의 여부

	SetWindowPos(_hWnd, NULL, x, y, (rc.right - rc.left),
		(rc.bottom - rc.top), SWP_NOZORDER | SWP_NOMOVE);
}