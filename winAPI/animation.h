#pragma once

#include <vector>

//==============================================
//20140718 ## animation class ##
//==============================================
//미중년 기억함... (모히또바~~)
//미연시 게또레이~~~~~
//은종 멘붕~~~

//2014년 8월 27일
//미틴 튀김 소보루~~~~ 따뜻함...

typedef void (*CALLBACK_FUNCTION)(void);
typedef void (*CALLBACK_FUNCTION_PARAMETER)(void*);

class animation
{
private:
	typedef std::vector<POINT> frameList;
	typedef std::vector<int> playList;

private:
	int _frameNum;				//프레임 수

	frameList _vFrameList;		//프레임 위치 리스트
	playList _vPlayList;		//플레이 리스트

	int _frameWidth;			//프레임 가로크기
	int _frameHeight;			//프레임 세로크기

	BOOL _loop;					//돌꺼냐?
	float _frameUpdateSec;		//프레임 업데이트(초 단위)
	float _elapsedSec;			//지난 프레임 (초 단위) 

	DWORD _nowPlayIdx;			//지금 플레이 하고 있는 인덱쓰~~
	BOOL _play;					//재생중이냐?		

	void* _obj;

	CALLBACK_FUNCTION_PARAMETER _callbackFunctionParameter;
	CALLBACK_FUNCTION _callbackFunction;

public:
	HRESULT init(int totalW, int totalH, int frameW, int frameH);						//이미지 총 가로세로크기와 한 프레임의 가로세로 크기로 셋팅
	void release(void);

	void setDefPlayFrame(BOOL reverse = NULL, BOOL loop = NULL);						//기본 셋팅
	void setDefPlayFrame(BOOL reverse, BOOL loop, CALLBACK_FUNCTION cbFunction);
	void setDefPlayFrame(BOOL reverse, BOOL loop, CALLBACK_FUNCTION_PARAMETER cbFunction, void* obj);
	
	void setPlayFrame(int* playArr, int arrLen, BOOL loop = NULL);					//플레이 프레임 배열로 셋팅
	void setPlayFrame(int* playArr, int arrLen, BOOL loop, CALLBACK_FUNCTION cbFunction);
	void setPlayFrame(int* playArr, int arrLen, BOOL loop, CALLBACK_FUNCTION_PARAMETER cbFunction, void* obj);
	
	void setPlayFrame(int start, int end, BOOL reverse = NULL, BOOL loop = NULL);		//플레이 프레임 시작과 종료로 셋팅
	void setPlayFrame(int start, int end, BOOL reverse, BOOL loop, CALLBACK_FUNCTION cbFunction);
	void setPlayFrame(int start, int end, BOOL reverse, BOOL loop, CALLBACK_FUNCTION_PARAMETER cbFunction, void* obj);

	void setFPS(int framePerSec);				//초당 갱신 횟수

	void frameUpdate(float elpasedTime);		//프레임 업데이트 타임

	void start(void);			//플레이 시작
	void stop(void);			//플레이 완전 정지
	void pause(void);			//플레이 일시 정시
	void resume(void);			//플레이 다시 시작

	//플레이 여부 확인
	inline BOOL isPlay(void) {return _play;}

	//프레임 위치를 얻는다.
	inline POINT getFramePos(void) {return _vFrameList[_vPlayList[_nowPlayIdx]];}

	//프래임 가로 크기를 얻는다.
	inline int getFrameWidth(void) {return _frameWidth;}

	//프레임 세로 크기를 얻는다.
	inline int getFrameHeight(void)	{return _frameHeight;}

	animation(void);
	~animation(void);
};
