#pragma once
#include "gamenode.h"
#include "aStarTest.h"

class aStarTestScene : public gameNode
{
private:
	aStarTest* _ast;

public:
	HRESULT init(void);
	void release(void);
	void update(void);
	void render(void);

	aStarTestScene(void);
	~aStarTestScene(void);
};
