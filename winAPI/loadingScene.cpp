#include "StdAfx.h"
#include "loadingScene.h"

loadingScene::loadingScene(void)
{
}

loadingScene::~loadingScene(void)
{
}

HRESULT loadingScene::init(void)
{
	gameNode::init();

	_loading = new loading;
	_loading->init();

	_loading->loadImage("move0", "move0.bmp", 8960, 4608, true, RGB(255, 0, 255));
	_loading->loadSound("Dengue Fever3", "Dengue Fever - Integration.mp3", false, false);
	_loading->loadImage("move1", "move0.bmp", 8960, 4608, true, RGB(255, 0, 255));
	_loading->loadImage("move2", "move0.bmp", 8960, 4608, true, RGB(255, 0, 255));
	_loading->loadImage("move3", "move0.bmp", 8960, 4608, true, RGB(255, 0, 255));
	_loading->loadImage("move4", "move0.bmp", 8960, 4608, true, RGB(255, 0, 255));
	_loading->loadImage("move5", "move0.bmp", 8960, 4608, true, RGB(255, 0, 255));
	_loading->loadSound("Dengue Fever4", "Dengue Fever - Integration.mp3", false, false);
	_loading->loadImage("move6", "move0.bmp", 8960, 4608, true, RGB(255, 0, 255));
	_loading->loadImage("move7", "move0.bmp", 8960, 4608, true, RGB(255, 0, 255));
	_loading->loadImage("move8", "move0.bmp", 8960, 4608, true, RGB(255, 0, 255));
	_loading->loadImage("move9", "move0.bmp", 8960, 4608, true, RGB(255, 0, 255));
	_loading->loadImage("move10", "move1.bmp", 6400, 5632, true, RGB(255, 0, 255));

	_loading->loadSound("Dengue Fever0", "Dengue Fever - Integration.mp3", false, false);
	_loading->loadImage("move11", "move1.bmp", 6400, 5632, true, RGB(255, 0, 255));
	_loading->loadImage("move12", "move1.bmp", 6400, 5632, true, RGB(255, 0, 255));
	_loading->loadImage("move13", "move1.bmp", 6400, 5632, true, RGB(255, 0, 255));
	_loading->loadImage("move14", "move1.bmp", 6400, 5632, true, RGB(255, 0, 255));
	_loading->loadSound("Dengue Fever1", "Dengue Fever - Integration.mp3", false, false);
	_loading->loadImage("move15", "move1.bmp", 6400, 5632, true, RGB(255, 0, 255));
	_loading->loadImage("move16", "move1.bmp", 6400, 5632, true, RGB(255, 0, 255));


	return S_OK;
}

void loadingScene::release(void)
{
	gameNode::release();

	_loading->release();
	SAFE_DELETE(_loading);
}	

void loadingScene::update(void)
{
	gameNode::update();

	_loading->update();

	if (!_loading->loadNext())
	{
		SCENEMANAGER->changeScene("ghotsnGoblinsScene");
	}
}

void loadingScene::render(void)
{
	_loading->render();
}





