#include "StdAfx.h"
#include "action.h"

action::action(void)
{
}

action::~action(void)
{
}

HRESULT action::init(void)
{
	gameNode::init();

	_callbackFunction = NULL;
	_isMoving = false;
	_worldTimeCount = 0.0f;

	return S_OK;
}

void action::release(void)
{
	gameNode::release();
}

void action::update(void)
{
	gameNode::update();

	moving();
}

void action::render(void)
{
	
}

void action::moveTo(image* img, float endX, float endY, float time)
{
	if (!_isMoving)
	{
		_callbackFunctionParameter = NULL;
		_callbackFunction = NULL;
		_obj = NULL;

		//이미지 주소값 담아둔다
		_image = img;

		//시작점
		_startX = img->getX();
		_startY = img->getY();

		//도착점
		_endX = endX;
		_endY = endY;

		//이동거리 구한다
		_travleRange = getDistance(_startX, _startY, _endX, _endY);

		//각도 구한다
		_angle = getAngle(_startX, _startY, _endX, _endY);

		//월드 타임 저장
		_worldTimeCount = TIMEMANAGER->getWorldTime();

		//이동시간
		_time = time;

		_isMoving = true;
	}
}

void action::moveTo(image* img, float endX, float endY, float time, CALLBACK_FUNCTION cbFunction)
{
	if (!_isMoving)
	{
		_callbackFunctionParameter = NULL;
		_callbackFunction = static_cast<CALLBACK_FUNCTION>(cbFunction);
		_obj = NULL;

		//이미지 주소값 담아둔다
		_image = img;

		//시작점
		_startX = img->getX();
		_startY = img->getY();

		//도착점
		_endX = endX;
		_endY = endY;

		//이동거리 구한다
		_travleRange = getDistance(_startX, _startY, _endX, _endY);

		//각도 구한다
		_angle = getAngle(_startX, _startY, _endX, _endY);

		//월드 타임 저장
		_worldTimeCount = TIMEMANAGER->getWorldTime();

		//이동시간
		_time = time;

		_isMoving = true;
	}
}

void action::moveTo(image* img, float endX, float endY, float time, CALLBACK_FUNCTION_PARAMETER cbFunction, void* obj)
{
	if (!_isMoving)
	{
		_callbackFunctionParameter = static_cast<CALLBACK_FUNCTION_PARAMETER>(cbFunction);
		_callbackFunction = NULL;
		_obj = obj;

		//이미지 주소값 담아둔다
		_image = img;

		//시작점
		_startX = img->getX();
		_startY = img->getY();

		//도착점
		_endX = endX;
		_endY = endY;

		//이동거리 구한다
		_travleRange = getDistance(_startX, _startY, _endX, _endY);

		//각도 구한다
		_angle = getAngle(_startX, _startY, _endX, _endY);

		//월드 타임 저장
		_worldTimeCount = TIMEMANAGER->getWorldTime();

		//이동시간
		_time = time;

		_isMoving = true;
	}
}

void action::moving(void)
{
	if (!_isMoving) return;

	float elpasedTime = TIMEMANAGER->getElapsedTime();
	float moveSpeed = (elpasedTime / _time) * _travleRange;

	//이동~~
	_image->setX(_image->getX() + cosf(_angle) * moveSpeed);
	_image->setY(_image->getY() + (-sinf(_angle) * moveSpeed));

	float time = TIMEMANAGER->getElapsedTime();
	if (_time + _worldTimeCount <= TIMEMANAGER->getWorldTime())
	{
		_worldTimeCount = TIMEMANAGER->getWorldTime();
		_image->setX(_endX);
		_image->setY(_endY);
		_isMoving = false;

		//콜백 호출
		if (_obj == NULL)
		{
			if (_callbackFunction != NULL) _callbackFunction();
		}
		else
		{
			if (_callbackFunctionParameter != NULL) _callbackFunctionParameter(_obj);
		}
	}
}


