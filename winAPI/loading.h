#pragma once

#include "gamenode.h"
#include "progressBar.h"
#include <vector>
#include <string>


enum LOADING_KIND
{
	LOADING_KIND_IMAGE_00,			//아무것도 없는 이미지
	LOADING_KIND_IMAGE_01,			//x, y값 있는 이미지
	LOADING_KIND_IMAGE_02,			//x, y값 없는 이미지
	LOADING_KIND_FRAMEIMAGE_00,	//x, y값 있는 이미지
	LOADING_KIND_FRAMEIMAGE_01,	//x, y값 없는 이미지
	LOADING_KIND_SOUND_00,			//사운드
	LOADING_KIND_END
};

//이미지 리소스 구조체
struct tagImageResource
{
	string keyName;
	const char* fileName;
	int x, y;
	int width, height;
	int frameX, frameY;
	bool trans;
	COLORREF transColor;
};

//사운드 리소스 구조체
struct tagSoundResource
{
	string keyName;
	string soundName;
	bool background;
	bool loop;
};

//로드 아이템
class loadItem
{
private:
	LOADING_KIND _kind;

	tagImageResource _imageResource;
	tagSoundResource _soundResource;
	
public:
	//이미지 로드 아이템
	HRESULT initForImage(string keyName, int width, int height);
	HRESULT initForImage(string keyName, const char* fileName, int x, int y, int width, int height, bool trans, COLORREF transColor);
	HRESULT initForImage(string keyName, const char* fileName, int width, int height, bool trans, COLORREF transColor);
	HRESULT initForFrameImage(string keyName, const char* fileName, int x, int y, int width, int height, int frameX, int frameY, bool trans, COLORREF transColor);
	HRESULT initForFrameImage(string keyName, const char* fileName, int width, int height, int frameX, int frameY, bool trans, COLORREF transColor);

	//사운드 로드 아이템
	HRESULT initForSound(string keyName, string soundName, bool background, bool loop);

	void release(void);

	//어떤종류의 데이터인지...
	LOADING_KIND getKind(void) {return _kind;}

	tagImageResource getImageResouce(void) {return _imageResource;}
	tagSoundResource getSoundResouce(void) {return _soundResource;}

	loadItem(void);
	~loadItem(void);
};


class loading : public gameNode
{
private:
	typedef vector<loadItem*> arrLoadItem;
	typedef vector<loadItem*>::iterator arrLoadItemIter;

private:
	arrLoadItem _vLoadItem;

	image* _background;
	progressBar* _loadingBar;

	int _current;

public:
	virtual HRESULT init(void);
	virtual void release(void);
	virtual void update(void);
	virtual void render(void);

	void loadImage(string keyName, int width, int height);
	void loadImage(string keyName, const char* fileName, int x, int y, int width, int height, bool trans, COLORREF transColor);
	void loadImage(string keyName, const char* fileName, int width, int height, bool trans, COLORREF transColor);
	void loadFrameImage(string keyName, const char* fileName, int x, int y, int width, int height, int frameX, int frameY, bool trans, COLORREF transColor);
	void loadFrameImage(string keyName, const char* fileName, int width, int height, int frameX, int frameY, bool trans, COLORREF transColor);
	void loadSound(string keyName, const char* fileName, bool background, bool loop);

	bool loadNext(void);

	loading(void);
	virtual ~loading(void);
};
