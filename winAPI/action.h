#pragma once

#include "gamenode.h"

class action : public gameNode
{
private:
	image* _image;
	float _startX, _startY;
	float _endX, _endY;

	float _angle;
	float _travleRange;
	float _time;
	float _worldTimeCount;

	bool _isMoving;

	CALLBACK_FUNCTION _callbackFunction;
	CALLBACK_FUNCTION_PARAMETER _callbackFunctionParameter;

	void* _obj;

public:
	virtual HRESULT init(void);
	virtual void release(void);
	virtual void update(void);
	virtual void render(void);

	void moveTo(image* img, float endX, float endY, float time);
	void moveTo(image* img, float endX, float endY, float time, CALLBACK_FUNCTION cbFunction);
	void moveTo(image* img, float endX, float endY, float time, CALLBACK_FUNCTION_PARAMETER cbFunction, void* obj);
	void moving(void);

	bool getInMoving(void) {return _isMoving;}

	action(void);
	~action(void);
};
