#pragma once

#include "gamenode.h"
#include "globalTank.h"

class tankMap : public gameNode
{
private:
	tagMap _map[TILEX * TILEY];
	DWORD _attribute[TILEX * TILEY];
	int _pos[2];

public:
	virtual HRESULT init(void);
	virtual void release(void);
	virtual void update(void);
	virtual void render(void);

	void load(void);

	DWORD* getAttribute(void) {return _attribute;}
	tagMap* getMap(void) {return _map;}
	int getPosFirst(void) {return _pos[0];}
	int getPosSecond(void) {return _pos[1];}

	tankMap(void);
	~tankMap(void);
};
