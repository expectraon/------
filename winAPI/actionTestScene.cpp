#include "StdAfx.h"
#include "actionTestScene.h"

actionTestScene::actionTestScene(void)
{
}

actionTestScene::~actionTestScene(void)
{
}

HRESULT actionTestScene::init(void)
{
	gameNode::init();

	_actionTest = new actionTest;
	_actionTest->init();

	return S_OK;
}

void actionTestScene::release(void)
{
	gameNode::release();

	SAFE_DELETE(_actionTest);
}

void actionTestScene::update(void)
{
	gameNode::update();

	_actionTest->update();
}

void actionTestScene::render(void)
{
	_actionTest->render();
}
