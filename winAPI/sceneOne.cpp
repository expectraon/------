#include "StdAfx.h"
#include "sceneOne.h"

sceneOne::sceneOne(void)
{
}

sceneOne::~sceneOne(void)
{
}

HRESULT sceneOne::init(void)
{
	gameNode::init();

	wsprintf(_text, "����� SceneOne��!!");

	_button = new button;
	_button->init("button", WINSIZEX / 2, WINSIZEY / 2 - 50, PointMake(0, 1), PointMake(0, 0), nextScene);

	return S_OK;
}

void sceneOne::release(void)
{
	gameNode::release();

	SAFE_DELETE(_button);
}

void sceneOne::nextScene(void)
{
	SCENEMANAGER->changeScene("sceneTwo");
}

void sceneOne::update(void)
{
	gameNode::update();

	_button->update();
}

void sceneOne::render(void)
{
	IMAGEMANAGER->findImage("background")->render(getMemDC(), 0, 0);

	_button->render();

	TextOut(getMemDC(), WINSIZEX / 2, WINSIZEY / 2, _text, strlen(_text));
}
