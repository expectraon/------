#include "StdAfx.h"
#include "timer.h"
#include <MMSystem.h>

//TimeGetTime 함수를 사용하기 위한 라이브러리
#pragma comment(lib, "winmm.lib")

//초기화
HRESULT timer::init(void)
{
	//고성능 타이머 지원되냐?
	//고성능 타이머 지원되면 _periodFrequency값에 초당 파악할수 있는 값이 들어온다.
	//예를 들어 초당 밀리세컨드까지 계산이 가능하면 1000값이 들어간다.
	if (QueryPerformanceFrequency((LARGE_INTEGER*)&_periodFrequency))
	{
		_isHardWare = true;
		QueryPerformanceCounter((LARGE_INTEGER*)&_lastTime);

		//초당시간 계산
		_timeScale = 1.0f / _periodFrequency;
	}
	else
	{
		_isHardWare = false;
		_lastTime = timeGetTime(); // timeGetTime() 밀리세컨단위의 시간
		_timeScale = 0.001f;
	}

	_frameRate = 0;
	_FPSFrameCount = 0;
	_FPSTimeElapsed = 0.0f;
	_worldTime = 0.0f;

	return S_OK;
}

//현재시간 계산
void timer::tick(float lockFPS)
{
	//현재 시간을 얻어온다
	if (_isHardWare)
	{
		//초정밀도 마이크로 초~~~!!!
		QueryPerformanceCounter((LARGE_INTEGER*)&_curTime);
	}
	else
	{
		//지원하지 않으면 time함수 사용...
		_curTime = timeGetTime();
	}

	//마지막 시간과 현재시간의 경과량을 측정한다.
	_timeElapsed = (_curTime - _lastTime) * _timeScale;

	//고정 프레임 처리한다
	if (lockFPS > 0.0f)
	{
		//고정 프레임의 시간을 충족할때까지 루프~~~~~
		while (_timeElapsed < (1.0f / lockFPS))
		{
			if (_isHardWare) QueryPerformanceCounter((LARGE_INTEGER*)&_curTime);
			else _curTime = timeGetTime();

			//마지막 시간과 현재시간의 경과량을 측정
			_timeElapsed = (_curTime - _lastTime) * _timeScale;
		}
	}

	//마지막 시간 기록~
	_lastTime = _curTime;

	//초당 프레임 카운트 증가
	_FPSFrameCount++;

	//초당 프레임 경과량 증가
	_FPSTimeElapsed += _timeElapsed;

	//전체 시간 경과량 증가
	_worldTime += _timeElapsed;

	//1초마다 프레임 초기화 한다.
	if (_FPSTimeElapsed > 1.0f)
	{
		_frameRate = _FPSFrameCount;
		_FPSFrameCount = 0;
		_FPSTimeElapsed = 0.0f;
	}
}

//현재 FPS를 얻어온다
unsigned long timer::getFrameRate(char* str) const
{
	if (str != NULL)
	{
		wsprintf(str, "FPS : %d", _frameRate);
	}
	return _frameRate;
}
