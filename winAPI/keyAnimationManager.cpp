#include "StdAfx.h"
#include "keyAnimationManager.h"
#include "image.h"
#include "animation.h"

keyAnimationManager::keyAnimationManager(void)
{
}

keyAnimationManager::~keyAnimationManager(void)
{
}

HRESULT keyAnimationManager::init(void)
{
	return S_OK;
}

void keyAnimationManager::release(void)
{
	deleteAll();
}

void keyAnimationManager::update(void)
{
	iterAnimation iter = _mTotalAnimation.begin();

	for (iter; iter != _mTotalAnimation.end(); ++iter)
	{	
		if (!iter->second->isPlay()) continue;
		iter->second->frameUpdate(TIMEMANAGER->getElapsedTime() * 1.0f);
	}
}

void keyAnimationManager::render(void)
{

}


//처음부터 그냥 애니메이션....
void keyAnimationManager::addDefaultAni(string keyName, char* imageName, int fps, bool reverse, bool loop)
{
	image* img = IMAGEMANAGER->findImage(imageName);
	animation* ani = new animation;

	ani->init(img->getWidth(), img->getHeight(), img->getFrameWidth(), img->getFrameHeight());
	ani->setDefPlayFrame(reverse, loop);
	ani->setFPS(fps);

	_mTotalAnimation.insert(make_pair(keyName, ani));
}

void keyAnimationManager::addDefaultAni(string keyName, char* imageName, int fps, bool reverse, bool loop, void* cbFunction)
{
	image* img = IMAGEMANAGER->findImage(imageName);
	animation* ani = new animation;

	ani->init(img->getWidth(), img->getHeight(), img->getFrameWidth(), img->getFrameHeight());
	ani->setDefPlayFrame(reverse, loop, (CALLBACK_FUNCTION)cbFunction);
	ani->setFPS(fps);

	_mTotalAnimation.insert(make_pair(keyName, ani));
}

void keyAnimationManager::addDefaultAni(string keyName, char* imageName, int fps, bool reverse, bool loop, void* cbFunction, void* obj)
{
	image* img = IMAGEMANAGER->findImage(imageName);
	animation* ani = new animation;

	ani->init(img->getWidth(), img->getHeight(), img->getFrameWidth(), img->getFrameHeight());
	ani->setDefPlayFrame(reverse, loop, (CALLBACK_FUNCTION_PARAMETER)cbFunction, obj);
	ani->setFPS(fps);

	_mTotalAnimation.insert(make_pair(keyName, ani));
}

//수동으로 선택한 애니메이션~~
void keyAnimationManager::addArrayAni(string keyName, char* imageName, int* arr, int arrLen, int fps, bool loop)
{
	image* img = IMAGEMANAGER->findImage(imageName);
	animation* ani = new animation;

	ani->init(img->getWidth(), img->getHeight(), img->getFrameWidth(), img->getFrameHeight());
	ani->setPlayFrame(arr, arrLen, loop);
	ani->setFPS(fps);

	_mTotalAnimation.insert(make_pair(keyName, ani));
}

void keyAnimationManager::addArrayAni(string keyName, char* imageName, int* arr, int arrLen, int fps, bool loop, void* cbFunction)
{
	image* img = IMAGEMANAGER->findImage(imageName);
	animation* ani = new animation;

	ani->init(img->getWidth(), img->getHeight(), img->getFrameWidth(), img->getFrameHeight());
	ani->setPlayFrame(arr, arrLen, loop, (CALLBACK_FUNCTION)cbFunction);
	ani->setFPS(fps);

	_mTotalAnimation.insert(make_pair(keyName, ani));
}

void keyAnimationManager::addArrayAni(string keyName, char* imageName, int* arr, int arrLen, int fps, bool loop, void* cbFunction, void* obj)
{
	image* img = IMAGEMANAGER->findImage(imageName);
	animation* ani = new animation;

	ani->init(img->getWidth(), img->getHeight(), img->getFrameWidth(), img->getFrameHeight());
	ani->setPlayFrame(arr, arrLen, loop, (CALLBACK_FUNCTION_PARAMETER)cbFunction, obj);
	ani->setFPS(fps);

	_mTotalAnimation.insert(make_pair(keyName, ani));
}

//시작과 끝의 좌표를 이용한 애니메이션~~~~~
void keyAnimationManager::addCoordinateAni(string keyName, char* imageName, int start, int end, int fps, bool reverse, bool loop)
{
	image* img = IMAGEMANAGER->findImage(imageName);
	animation* ani = new animation;

	ani->init(img->getWidth(), img->getHeight(), img->getFrameWidth(), img->getFrameHeight());
	ani->setPlayFrame(start, end, reverse, loop);
	ani->setFPS(fps);

	_mTotalAnimation.insert(make_pair(keyName, ani));
}

void keyAnimationManager::addCoordinateAni(string keyName, char* imageName, int start, int end, int fps, bool reverse, bool loop, void* cbFunction)
{
	image* img = IMAGEMANAGER->findImage(imageName);
	animation* ani = new animation;

	ani->init(img->getWidth(), img->getHeight(), img->getFrameWidth(), img->getFrameHeight());
	ani->setPlayFrame(start, end, reverse, loop, (CALLBACK_FUNCTION)cbFunction);
	ani->setFPS(fps);

	_mTotalAnimation.insert(make_pair(keyName, ani));
}

void keyAnimationManager::addCoordinateAni(string keyName, char* imageName, int start, int end, int fps, bool reverse, bool loop, void* cbFunction, void* obj)
{
	image* img = IMAGEMANAGER->findImage(imageName);
	animation* ani = new animation;

	ani->init(img->getWidth(), img->getHeight(), img->getFrameWidth(), img->getFrameHeight());
	ani->setPlayFrame(start, end, reverse, loop, (CALLBACK_FUNCTION_PARAMETER)cbFunction, obj);
	ani->setFPS(fps);

	_mTotalAnimation.insert(make_pair(keyName, ani));
}

void keyAnimationManager::start(string keyName)
{
	iterAnimation iter = _mTotalAnimation.find(keyName);
	iter->second->start();
}

void keyAnimationManager::stop(string keyName)
{
	iterAnimation iter = _mTotalAnimation.find(keyName);
	iter->second->stop();
}

void keyAnimationManager::pause(string keyName)
{
	iterAnimation iter = _mTotalAnimation.find(keyName);
	iter->second->pause();
}

void keyAnimationManager::resume(string keyName)
{
	iterAnimation iter = _mTotalAnimation.find(keyName);
	iter->second->resume();
}

//애니메이션 찾기
animation* keyAnimationManager::findAnimation(string keyName)
{
	iterAnimation iter = _mTotalAnimation.find(keyName);
	if (iter != _mTotalAnimation.end())
	{
		return iter->second;
	}
	return NULL;
}

//애니메이션 지운다
void keyAnimationManager::deleteAll(void)
{
	iterAnimation iter = _mTotalAnimation.begin();

	for (; iter != _mTotalAnimation.end();)
	{
		if (iter->second != NULL)
		{
			iter->second->release();
			SAFE_DELETE(iter->second);
			iter = _mTotalAnimation.erase(iter);
		}
		else
		{
			++iter;
		}
	}

	_mTotalAnimation.clear();
}
