#pragma once
#include "gamenode.h"

class ship : public gameNode
{
protected:
	image* _image;
	RECT _rc;
	float _x, _y;
	float _angle;
	float _speed;
	float _radius;

public:
	virtual HRESULT init(const char* imageName, int x, int y);
	virtual void release(void);
	virtual void update(void);
	virtual void render(void);

	//쉽 관련 가상화 함수
	virtual void draw(void);
	virtual void keyControl(void);
	virtual void move(void);

	//포커스 드로우
	virtual void focusDraw(void);
	
	//디폴트 드로우
	virtual void defaultDraw(RECT rcFocus);

	image* getImage(void) {return _image;}
	RECT getRect(void){return _rc;}

	ship(void);
	virtual ~ship(void);
};
