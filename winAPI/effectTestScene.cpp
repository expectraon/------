#include "StdAfx.h"
#include "effectTestScene.h"

effectTestScene::effectTestScene(void)
{
}

effectTestScene::~effectTestScene(void)
{
}

HRESULT effectTestScene::init(void)
{
	gameNode::init();

//	EFFECTMANAGER->addEffect("boom", "effectSample.bmp", 1040, 80, 80, 80, 1, 0.3f, 5);

	image* img = IMAGEMANAGER->addFrameImage("boom", "effectSample.bmp", 1040, 80, 13, 1, true, RGB(255, 0, 255)); 

	/*
	_fa1 = new frameAnimation;
	_fa1->init();
*/
	return S_OK;
}

void effectTestScene::release(void)
{
	gameNode::release();
}

void effectTestScene::update(void)
{
	gameNode::update();

	if (KEYMANAGER->isOnceKeyDown(VK_LBUTTON))
	{
		FAMANAGER->animationXFrame(IMAGEMANAGER->findImage("boom"), 13, 0.1f, false);
//		_fa1->animationXFrame(IMAGEMANAGER->findImage("boom"), 13, 0.1f, true);

//		EFFECTMANAGER->play("boom", _ptMouse.x, _ptMouse.y);
	}

	EFFECTMANAGER->update();

	FAMANAGER->update();
//	_fa1->update();
}

void effectTestScene::render(void)
{
	image* img = IMAGEMANAGER->findImage("boom");
	img->frameRender(getMemDC(), 100, 100, img->getFrameX(), img->getFrameY());

	EFFECTMANAGER->render();

//	_fa1->render();
}
