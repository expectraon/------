#pragma once
#include "singletonbase.h"
#include <vector>

class action;

class actionManager : public singletonBase <actionManager>
{
private:
	typedef vector<action*> arrAction;
	typedef vector<action*>::iterator iterAction;

private:
	arrAction _vAction;
	iterAction _viAction;

public:
	HRESULT init(void);
	void release(void);
	void update(void);
	void render(void);

	void moveTo(image* img, float endX, float endY, float time);
	void moveTo(image* img, float endX, float endY, float time, void* cbFunction);
	void moveTo(image* img, float endX, float endY, float time, void* cbFunction, void* obj);

	actionManager(void);
	~actionManager(void);
};
