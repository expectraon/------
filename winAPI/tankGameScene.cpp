#include "StdAfx.h"
#include "tankGameScene.h"

tankGameScene::tankGameScene(void)
{
}

tankGameScene::~tankGameScene(void)
{
}

HRESULT tankGameScene::init(void)
{
	gameNode::init();

	_tankMap = new tankMap;
	_tankMap->init();

	_tank = new tank;
	_tank->init();
	_tank->setTankMap(_tankMap);
	_tank->setTankPosition();

	return S_OK;
}

void tankGameScene::release(void)
{
	gameNode::release();

	SAFE_DELETE(_tankMap);
	SAFE_DELETE(_tank);
}

void tankGameScene::update(void)
{
	gameNode::update();

	_tankMap->update();
	_tank->update();
}

void tankGameScene::render(void)
{
	_tankMap->render();
	_tank->render();
}
