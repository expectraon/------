#pragma once

#include "gamenode.h"

class image;
class animation;

class effect : public gameNode
{
protected:
	int _x;		//����Ʈ ��ǥx
	int _y;		//����Ʈ ��ǥy

	image*		_effectImage;		//����Ʈ �̹���
	animation*	_effectAnimation;	//����Ʈ �ִϸ��̼�
	bool		_isRunning;			//����Ʈ ���ư����ֳ�?
	float		_elapsedTime;		//����Ʈ ����ð�

public:
	virtual HRESULT init(image* effectIamge, int frameW, int frameH, int fps, float elapsedTime);
	virtual void release(void);
	virtual void render(void);
	virtual void update(void);

	void startEffect(int x, int y);	//����Ʈ ����
	virtual void killEffect(void);	//����Ʈ ����

	BOOL getIsRunning(void) {return _isRunning;}

	effect(void);
	virtual ~effect(void);
};
