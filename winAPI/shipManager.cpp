#include "StdAfx.h"
#include "shipManager.h"

shipManager::shipManager(void)
{
}

shipManager::~shipManager(void)
{
}

HRESULT shipManager::init(void)
{
	gameNode::init();

	//초기화
	_ship[SHIPKIND_CARRIER] = new carrier;
	_ship[SHIPKIND_CARRIER]->init("carrier", 100, 100);

	_ship[SHIPKIND_CORSAIR] = new corsair;
	_ship[SHIPKIND_CORSAIR]->init("corsair", 0, 0);

	//포커스 쉽 설정
	_focusShip = _ship[SHIPKIND_CARRIER];

	//스페이스 클래스에 포커스 쉽 넣어준다
	_space = new space;
	_space->init();
	_space->setShip(_focusShip);

	return S_OK;
}

void shipManager::release(void)
{
	gameNode::release();

	for (int i = 0; i < SHIPKIND_END; i++)
	{
		_ship[i]->release();
		SAFE_DELETE(_ship[i]);
	}

	_space->release();
	SAFE_DELETE(_space);
}

void shipManager::update(void)
{
	gameNode::update();

	//포커스 변환
	if (KEYMANAGER->isOnceKeyDown(VK_F1))
	{
		_focusShip = _ship[SHIPKIND_CARRIER];
		_space->setShip(_focusShip);
	}

	if (KEYMANAGER->isOnceKeyDown(VK_F2))
	{
		_focusShip = _ship[SHIPKIND_CORSAIR];
		_space->setShip(_focusShip);
	}

	//쉽 업데이트
	for (int i = 0; i < SHIPKIND_END; i++)
	{
		_ship[i]->update();
	}

	//현재 포커스로 맞춰진 쉽 키컨트롤
	_focusShip->keyControl();
}

void shipManager::render(void)
{
	_space->render();

	for (int i = 0; i < SHIPKIND_END; i++)
	{
		//포커스 쉽과 같으면...
		if (_focusShip == _ship[i])
		{
			_focusShip->focusDraw();
		}
		else
		{
			_ship[i]->defaultDraw(_focusShip->getRect());
		}
	}
}
