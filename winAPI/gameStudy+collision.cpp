﻿#include "StdAfx.h"
#include "gameStudy.h"

void gameStudy::collision(void)
{
	//적 총알과 플레이어 몸뚱아리
	for (int i = 0; i < _enemyManager->getBullet()->getVBullet().size(); i++)
	{
		RECT rc;
		if (IntersectRect(&rc, &collisionAreaResizing(_player->getRect(), 150, 60),
			&_enemyManager->getBullet()->getVBullet()[i].rc))
		{
			_enemyManager->getBullet()->removeBullet(i);
			_player->damage(10);
		}
	}

	//플레이어 총알과 적 몸뚱아리
	for (int i = 0; i < _player->getMissile()->getVBullet().size(); i++)
	{
		for (int j = 0; j < _enemyManager->getVMinion().size(); j++)
		{
			RECT rc;
			if (IntersectRect(&rc, &_player->getMissile()->getVBullet()[i].rc,
		//		&_enemyManager->getVMinion()[j]->getRect()))
		&collisionAreaResizing(_enemyManager->getVMinion()[j]->getRect(), 30, 5)))
			{
				_player->removeMissile(i);
				_enemyManager->removeMinion(j);
				break;
			}
		}
	}
}
