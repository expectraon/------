#include "StdAfx.h"
#include "gameStudy.h"

//2014.07.28
//미중년 아이스크림 사온다~~~~~~~~~~~~
//사온다~~~ 사온다~~ 사온~~~다~~~다`~~~~다

//2014.07.29
//반장 안옴...

//2014.08.01
//반장 미쳤음...

//2014.09.01
//반장 astar 못함..

//2014.09.10
//반장 귀에 돌 들었음...

//2014.09.16
//반장 겉으로는 정상으로 보임....

gameStudy::gameStudy(void)
{
}

gameStudy::~gameStudy(void)
{
}

HRESULT gameStudy::init(void)
{
	gameNode::init(true);

	//맵이미지
	IMAGEMANAGER->addImage("mapImage", "map.bmp", WINSIZEX, WINSIZEY, true, RGB(255, 0, 255));

	//백그라운드
	IMAGEMANAGER->addImage("background", "space.bmp", WINSIZEX, WINSIZEY);

	//벌거벗은 기사
	IMAGEMANAGER->addFrameImage("knight", "knight.bmp", 612, 312, 9, 4, true, RGB(255, 0, 255));

	//씬초기화
	gameNode* asts = new loadingScene;
	gameNode* gngc = new ghotsnGoblinsScene;
	gameNode* mlts = new memoryLeakTestScene;

	gameNode* Sg = new sadarimain;

	//씬 매니져에 추가한다
	SCENEMANAGER->addScene("loadingScene", asts);
	SCENEMANAGER->addScene("sadari", Sg);
	SCENEMANAGER->addScene("ghotsnGoblinsScene", gngc);
	SCENEMANAGER->addScene("memoryLeakTestScene", mlts);

	//현재 씬을 씬원으로~~~~~~~~~~~~~~~~~~~~
	SCENEMANAGER->changeScene("sadari");

	return S_OK;
}

void gameStudy::release(void)
{
	gameNode::release();
}

void gameStudy::update(void)
{
	gameNode::update();

	SCENEMANAGER->update();
}

void gameStudy::render(void)
{
	//맨 밑에 그냥 깔아두는 맵이미지
	IMAGEMANAGER->findImage("mapImage")->render(getMemDC(), 0, 0);

	//씬매니져
	SCENEMANAGER->render();

	//타임매니져
	TIMEMANAGER->render(getMemDC());

	//백버퍼를 화면버퍼로~~~
	this->getBackBuffer()->render(getHDC(), 0, 0);
}