#pragma once
#include "gamenode.h"

class progressBar : public gameNode
{
private:
	RECT _rcProgress;
	int _x;
	int _y;
	float _width;

	char* _keyBarBottom;
	char* _keyBarTop;

	image* _progressBarBottom;
	image* _progressBarTop;

public:
	virtual HRESULT init(int x, int y, int width, int height);
	virtual HRESULT progressBar::init(char* btnUpImageKeyName, char* btnDownImageKeyName, int x, int y, int width, int height);
	virtual void release(void);
	virtual void update(void);
	virtual void render(void);

	void setGauge(float currentGauge, float maxGauge);

	void setX(int x) {_x = x;}
	void setY(int y) {_y = y;}

	progressBar(void);
	~progressBar(void);
};
