#pragma once

#include "gamenode.h"
#include "action.h"

class actionTest : public gameNode
{
private:
	image* _image;
	float _x, _y;
	RECT _rc;

//	action* _moveTo;

public:
	virtual HRESULT init(void);
	virtual void release(void);
	virtual void update(void);
	virtual void render(void);

	static void moveEnd(void);

	actionTest(void);
	~actionTest(void);
};
