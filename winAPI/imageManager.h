#pragma once

//==============================================
//20140704 ## imageManager ##
//==============================================
//반장 제대하고 탈주 분위기.... (멘탈 탈주~)
//반장 자고 있음... 20140718

#include "singletonbase.h"
#include "image.h"
#include <map>
#include <string>

class imageManager : public singletonBase <imageManager>
{
public:
	//이미지 리스트 맵
	typedef std::map<std::string, image*> mapImageList;

	//이미지 리스트 이터레이터
	typedef std::map<std::string, image*>::iterator mapImageIter;

private:
	//맵으로 구현된 이미지 리스트
	mapImageList _mImageList;

public:
	imageManager(void);
	~imageManager(void);

	//초기화
	HRESULT init(void);

	//해제
	void release(void);

	//이미지 추가
	//키값 설정해서 추가해준다
	image* addImage(std::string strKey, int width, int height);
	image* addImage(std::string strKey, const DWORD resID, int width, int height, BOOL tran = FALSE, COLORREF transColor = RGB(0, 0, 0));
	image* addImage(std::string strKey, const char* fileName, int x, int y, int width, int height, BOOL tran = FALSE, COLORREF transColor = RGB(0, 0, 0));
	image* addImage(std::string strKey, const char* fileName, int width, int height, BOOL tran = FALSE, COLORREF transColor = RGB(0, 0, 0));
	
	//키값 + 프레임 설정
	image* addFrameImage(std::string strKey, const char* fileName, int x, int y, int width, int height, 
		int frameX, int frameY, BOOL tran = FALSE, COLORREF transColor = RGB(0, 0, 0));

	image* addFrameImage(std::string strKey, const char* fileName, int width, int height, 
		int frameX, int frameY, BOOL tran = FALSE, COLORREF transColor = RGB(0, 0, 0));

	//일반 렌더
	void imageManager::render(std::string strKey, HDC hdc);
	void imageManager::render(std::string strKey, HDC hdc, int destX, int destY);
	void imageManager::render(std::string strKey, HDC hdc, int sourX, int sourY, int sourWidth, int sourHeight);
	void imageManager::render(std::string strKey, HDC hdc, int destX, int destY, int sourX, int sourY, int sourWidth, int sourHeight);

	//프레임렌더
	void imageManager::frameRender(std::string strKey, HDC hdc, int destX, int destY);
	void imageManager::frameRender(std::string strKey, HDC hdc, int destX, int destY, int frameX, int frameY);

	//루프 렌더
	void loopRender(std::string strKey, HDC hdc, const LPRECT drawArea, int offsetX, int offsetY);
	
	//이미지 찾기
	image* findImage(std::string strKey);

	//삭제
	BOOL deleteImage(std::string strKey);

	//전체삭제
	BOOL deleteAll(void);
};
