#include "StdAfx.h"
#include "imageManager.h"

imageManager::imageManager(void)
{
}

imageManager::~imageManager(void)
{
}

//초기화
HRESULT imageManager::init(void)
{
	return S_OK;
}

//해제
void imageManager::release(void)
{
	deleteAll();
}

//이미지 추가
//키값 설정해서 추가해준다
image* imageManager::addImage(std::string strKey, int width, int height)
{
	//추가하려는 키 값의 이미지가 있는지 확인한다
	image* img = findImage(strKey);

	//추가하려는 이미지가 있으면 추가하지 않고 기존의 이미지를 리턴한다
	if (img) return img;

	//중복 이미지 없으면 생성
	img = new image;

	//이미지 셋팅하고 이미지가 제대로 초기화 되지 않았으면 리턴 널~~~~~~
	if (FAILED(img->init(width, height)))
	{
		SAFE_DELETE(img);
		return NULL;
	}

	//생성된 이미지 map리스트에 추가한다
	_mImageList.insert(std::make_pair(strKey, img));

	return img;
}

image* imageManager::addImage(std::string strKey, const DWORD resID, int width, int height,
	BOOL tran, COLORREF transColor)
{
	//추가하려는 키 값의 이미지가 있는지 확인한다
	image* img = findImage(strKey);

	//추가하려는 이미지가 있으면 추가하지 않고 기존의 이미지를 리턴한다
	if (img) return img;

	//중복 이미지 없으면 생성
	img = new image;

	//이미지 셋팅하고 이미지가 제대로 초기화 되지 않았으면 리턴 널~~~~~~
	if (FAILED(img->init(resID, width, height, tran, transColor)))
	{
		SAFE_DELETE(img);
		return NULL;
	}

	//생성된 이미지 map리스트에 추가한다
	_mImageList.insert(std::make_pair(strKey, img));

	return img;
}

image* imageManager::addImage(std::string strKey, const char* fileName, int x, int y, 
	int width, int height, BOOL tran, COLORREF transColor)
{
	//추가하려는 키 값의 이미지가 있는지 확인한다
	image* img = findImage(strKey);

	//추가하려는 이미지가 있으면 추가하지 않고 기존의 이미지를 리턴한다
	if (img) return img;

	//중복 이미지 없으면 생성
	img = new image;

	//이미지 셋팅하고 이미지가 제대로 초기화 되지 않았으면 리턴 널~~~~~~
	if (FAILED(img->init(fileName, x, y, width, height, tran, transColor)))
	{
		SAFE_DELETE(img);
		return NULL;
	}

	//생성된 이미지 map리스트에 추가한다
	_mImageList.insert(std::make_pair(strKey, img));

	return img;
}

image* imageManager::addImage(std::string strKey, const char* fileName, 
	int width, int height, BOOL tran, COLORREF transColor)
{
	//추가하려는 키 값의 이미지가 있는지 확인한다
	image* img = findImage(strKey);

	//추가하려는 이미지가 있으면 추가하지 않고 기존의 이미지를 리턴한다
	if (img) return img;

	//중복 이미지 없으면 생성
	img = new image;

	//이미지 셋팅하고 이미지가 제대로 초기화 되지 않았으면 리턴 널~~~~~~
	if (FAILED(img->init(fileName, width, height, tran, transColor)))
	{
		SAFE_DELETE(img);
		return NULL;
	}

	//생성된 이미지 map리스트에 추가한다
	_mImageList.insert(std::make_pair(strKey, img));

	return img;
}

//키값 + 프레임 설정
image* imageManager::addFrameImage(std::string strKey, const char* fileName, int x, int y, int width, int height, 
	int frameX, int frameY, BOOL tran, COLORREF transColor)
{
	//추가하려는 키 값의 이미지가 있는지 확인한다
	image* img = findImage(strKey);

	//추가하려는 이미지가 있으면 추가하지 않고 기존의 이미지를 리턴한다
	if (img) return img;

	//중복 이미지 없으면 생성
	img = new image;

	//이미지 셋팅하고 이미지가 제대로 초기화 되지 않았으면 리턴 널~~~~~~
	if (FAILED(img->initFrame(fileName, x, y, width, height, frameX, frameY, tran, transColor)))
	{
		SAFE_DELETE(img);
		return NULL;
	}

	//생성된 이미지 map리스트에 추가한다
	_mImageList.insert(std::make_pair(strKey, img));

	return img;
}

image* imageManager::addFrameImage(std::string strKey, const char* fileName, int width, int height, 
	int frameX, int frameY, BOOL tran, COLORREF transColor)
{
	//추가하려는 키 값의 이미지가 있는지 확인한다
	image* img = findImage(strKey);

	//추가하려는 이미지가 있으면 추가하지 않고 기존의 이미지를 리턴한다
	if (img) return img;

	//중복 이미지 없으면 생성
	img = new image;

	//이미지 셋팅하고 이미지가 제대로 초기화 되지 않았으면 리턴 널~~~~~~
	if (FAILED(img->initFrame(fileName, width, height, frameX, frameY, tran, transColor)))
	{
		SAFE_DELETE(img);
		return NULL;
	}

	//생성된 이미지 map리스트에 추가한다
	_mImageList.insert(std::make_pair(strKey, img));

	return img;
}

//이미지 찾기
image* imageManager::findImage(std::string strKey)
{
	//해당 키를 검색한다
	mapImageIter key = _mImageList.find(strKey);

	//해당 키값을 찾았으면..
	if (key != _mImageList.end())
	{
		return key->second;
	}

	return NULL;
}

//삭제
BOOL imageManager::deleteImage(std::string strKey)
{
	//해당 키를 검색한다
	mapImageIter key = _mImageList.find(strKey);

	//키값을 찾았으면..
	if (key != _mImageList.end())
	{
		//이미지 해제
		key->second->release();

		//메모리 해제
		SAFE_DELETE(key->second);

		//맵리스트에서 삭제한다
		_mImageList.erase(key);

		return TRUE;
	}

	return FALSE;
}

//전체삭제
BOOL imageManager::deleteAll(void)
{
	//해당 키를 검색한다
	mapImageIter iter = _mImageList.begin();

	//키값을 찾았으면..
	for (; iter != _mImageList.end();)
	{
		//지워지면 반복자를 증가하지 않는다
		if (iter->second != NULL)
		{
			iter->second->release();
			delete iter->second;
			iter = _mImageList.erase(iter);
		}
		//반복자 증가~~
		else
		{
			++iter;
		}
	}

	_mImageList.clear();

	return TRUE;
}

//렌더 (렌더할 이미지의 키 값 필요)

//일반 렌더
void imageManager::render(std::string strKey, HDC hdc)
{
	image* img = findImage(strKey);

	if(img) img->render(hdc);
}

void imageManager::render(std::string strKey, HDC hdc, int destX, int destY)
{
	image* img = findImage(strKey);

	if(img) img->render(hdc, destX, destY);
}

void imageManager::render(std::string strKey, HDC hdc, int sourX, int sourY, int sourWidth, int sourHeight)
{
	image* img = findImage(strKey);

	if(img) img->render(hdc, sourX, sourY, sourWidth, sourHeight);
}

void imageManager::render(std::string strKey, HDC hdc, int destX, int destY, int sourX, int sourY, int sourWidth, int sourHeight)
{
	image* img = findImage(strKey);

	if(img) img->render( hdc, destX, destY, sourX, sourY, sourWidth, sourHeight );
}

//프레임렌더
void imageManager::frameRender(std::string strKey, HDC hdc, int destX, int destY)
{
	image* img = findImage(strKey);

	if(img) img->frameRender(hdc, destX, destY);
}

void imageManager::frameRender(std::string strKey, HDC hdc, int destX, int destY, int frameX, int frameY)
{
	image* img = findImage(strKey);

	if(img) img->frameRender(hdc, destX, destY, frameX, frameY);
}

//루프 렌더
void imageManager::loopRender(std::string strKey, HDC hdc, const LPRECT drawArea, int offsetX, int offsetY)
{
	image* img = findImage(strKey);

	if(img) img->loopRender(hdc, drawArea, offsetX, offsetY);
}


