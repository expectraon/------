#include "StdAfx.h"
#include "corsair.h"

corsair::corsair(void)
{
}

corsair::~corsair(void)
{
}

void corsair::keyControl(void)
{
	if (KEYMANAGER->isStayKeyDown('A'))
	{
		_angle += 0.05f;
		if (_angle > PI * 2) _angle -= PI * 2;
	}

	if (KEYMANAGER->isStayKeyDown('D'))
	{
		_angle -= 0.05f;
		if (_angle < 0) _angle += PI * 2;
	}

	if (KEYMANAGER->isStayKeyDown('W'))
	{
		_speed += 0.02;
	}
	if (KEYMANAGER->isStayKeyDown('S'))
	{
		_speed -= 0.02;
	}
}
