#include "StdAfx.h"
#include "tile.h"
#include "aStarTest.h"

tile::tile(void)
: _idX(0),
_idY(0),
_totalCost(0),
_costFromStart(0),
_costToGoal(0),
_parentNode(NULL)
{
}

tile::~tile(void)
{
}

HRESULT tile::init(int idX, int idY)
{
	gameNode::init();

	_idX = idX;
	_idY = idY;

	_color = RGB(250, 150, 0);
	_brush = CreateSolidBrush(_color);
	_pen = CreatePen(PS_SOLID, 1, RGB(0, 0, 0));
	_center = PointMake(idX * TILEWIDTH + (TILEWIDTH / 2), 
		idY * TILEHEIGHT + (TILEHEIGHT / 2));
	_rc = RectMakeCenter(_center.x, _center.y, TILEWIDTH, TILEHEIGHT);

	return S_OK;
}

void tile::release(void)
{
	gameNode::release();
	DeleteObject(_brush);
	DeleteObject(_pen);
}

void tile::update(void)
{
	gameNode::update();
}

void tile::render(void)
{
	//사각형 그리기
	SelectObject(getMemDC(), _brush);
	FillRect(getMemDC(), &_rc, _brush);

	//라인 그리기
	SelectObject(getMemDC(), _pen);
	Rectangle(getMemDC(), _rc.left, _rc.top, 
		_rc.right, _rc.bottom);
}
