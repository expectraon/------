#include "StdAfx.h"
#include "progressBar.h"

progressBar::progressBar(void)
{
}

progressBar::~progressBar(void)
{
}

HRESULT progressBar::init(int x, int y, int width, int height)
{
	gameNode::init();

	_x = x;
	_y = y;

	_rcProgress = RectMake(x, y, width, height);

	_progressBarBottom = IMAGEMANAGER->addImage("barBottom", "barBottom.bmp", width, height, true, RGB(255, 0, 255));
	_progressBarTop = IMAGEMANAGER->addImage("barTop", "barTop.bmp", width, height, true, RGB(255, 0, 255));

	_width = _progressBarTop->getWidth();

	return S_OK;
}

HRESULT progressBar::init(char* btnUpImageKeyName, char* btnDownImageKeyName, int x, int y, int width, int height)
{
	gameNode::init();

	_keyBarBottom = btnDownImageKeyName;
	_keyBarTop = btnUpImageKeyName;

	_x = x;
	_y = y;

	_rcProgress = RectMake(x, y, width, height);

	char strUpKey[128];
	char strUpName[128];
	char strDownKey[128];
	char strDownName[128];

	ZeroMemory(strUpKey, sizeof(strUpKey));
	ZeroMemory(strUpName, sizeof(strUpName));
	ZeroMemory(strDownKey, sizeof(strDownKey));
	ZeroMemory(strDownName, sizeof(strDownName));

	sprintf(strUpKey, btnUpImageKeyName);
	sprintf(strUpName, "%s.bmp", btnUpImageKeyName);
	sprintf(strDownKey, btnDownImageKeyName);
	sprintf(strDownName, "%s.bmp", btnDownImageKeyName);

	_progressBarBottom = IMAGEMANAGER->addImage(strDownKey, strDownName, x, y, width, height, true, RGB(255, 0, 255));
	_progressBarTop = IMAGEMANAGER->addImage(strUpKey, strUpName, x, y, width, height, true, RGB(255, 0, 255));

	_width = _progressBarTop->getWidth();

	return S_OK;
}

void progressBar::release(void)
{
	gameNode::release();
}

void progressBar::update(void)
{
	gameNode::update();

	_rcProgress = RectMakeCenter(_x + _progressBarBottom->getWidth() / 2, 
		_y + _progressBarBottom->getHeight() / 2, 
		_progressBarBottom->getWidth(), _progressBarBottom->getHeight());
}

void progressBar::render(void)
{
	IMAGEMANAGER->render(_keyBarBottom, getMemDC(), _rcProgress.left,
		_y, 0, 0, _progressBarBottom->getWidth(), _progressBarBottom->getHeight());

	IMAGEMANAGER->render(_keyBarTop, getMemDC(), _rcProgress.left,
		_y, 0, 0, _width, _progressBarTop->getHeight());

	/*
	_progressBarBottom->render(getMemDC(), _x,
		_y, 0, 0, _progressBarBottom->getWidth(), 
		_progressBarBottom->getHeight());

	_progressBarTop->render(getMemDC(), _x,
		_y, 0, 0, _width, _progressBarTop->getHeight());*/
}

void progressBar::setGauge(float currentGauge, float maxGauge)
{
	_width = (currentGauge / maxGauge) * _progressBarBottom->getWidth();
}
