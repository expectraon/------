#pragma once
#include "gamenode.h"
#include "pixelCollision.h"

class pixelCollisionScene : public gameNode
{
private:
	pixelCollision* _pc;

public:
	virtual HRESULT init(void);
	virtual void release(void);
	virtual void update(void);
	virtual void render(void);

	pixelCollisionScene(void);
	~pixelCollisionScene(void);
};
