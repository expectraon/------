// stdafx.h : 자주 사용하지만 자주 변경되지는 않는
// 표준 시스템 포함 파일 및 프로젝트 관련 포함 파일이
// 들어 있는 포함 파일입니다.
//

#pragma once
#pragma warning (disable : 4244)
#pragma warning (disable : 4996)
//==============================================
//20140610 ## 헤더파일 써준다~ ##
//==============================================
#include "targetver.h"

#define WIN32_LEAN_AND_MEAN	//MFC가 아닌 응용 프로그램의 경우에는 WIN32_LEAN_AND_MEAN
							//를 정의해 빌드 시간을 단축시킨다 
							//commdlg.h등등 필요없는 헤더파일을 인클루드 하지 않는다

#include <windows.h>		//윈도우 헤더파일 인클루드
#include <stdio.h>			//스탠다드 입출력 헤더 인클루드 (printf, scanf, puts, gets 등등..)
#include <tchar.h>			//윈도우에서 사용할 문자열 출력 헤더 인클루드 (TextOut, DrawText, wsprintf등등..)
							//기본적으로 유니코드다 영어, 한글 모두 2바이트....
							//MBCS (Multi Byte Character Set) -> 기본에 사용하던 멀티바이트 형태의 문자열
							//WBCS (Wide Byte Character Set) -> 모든 문자를 2바이트로 처리, 유니코드 기반

#include "commonMacroFunction.h"
#include "randomFunction.h"
#include "keyManager.h"
#include "imageManager.h"
#include "timeManager.h"
#include "utils.h"
#include "txtDataManager.h"
#include "sceneManager.h"
#include "database.h"
#include "soundManager.h"
#include "effectManager.h"
#include "frameAnimationManager.h"
#include "keyAnimationManager.h"
#include "actionManager.h"
#include <crtdbg.h>

//=================================================
//20140611 ## 디파인문 써준다~ ## Thesday of Blood
//=================================================

using namespace MY_UTIL;

//윈도우 크기 및 스타일 설정

//#define FULLSCREEN
#define WINNAME (TEXT("APIWindow"))

#ifdef FULLSCREEN
	#define WINSTARTX 0
	#define WINSTARTY 0
	#define WINSIZEX GetSystemMetrics(SM_CXSCREEN)		
	#define WINSIZEY GetSystemMetrics(SM_CYSCREEN)
#else
	#define WINSTARTX 100
	#define WINSTARTY 0
	#define WINSIZEX 1024		
	#define WINSIZEY 768
#endif

#define WINSTYLE WS_CAPTION | WS_SYSMENU

//싱글톤 디파인 선언
#define KEYMANAGER keyManager::getSingleton()
#define RND randomFunction::getSingleton()
#define IMAGEMANAGER imageManager::getSingleton()
#define TIMEMANAGER timeManager::getSingleton()
#define TXTMANAGER txtDataManager::getSingleton()
#define SCENEMANAGER sceneManager::getSingleton()
#define DATABASE database::getSingleton()
#define SOUNDMANAGER soundManager::getSingleton()
#define EFFECTMANAGER effectManager::getSingleton()
#define FAMANAGER frameAnimationManager::getSingleton()
#define KEYANIMANAGER keyAnimationManager::getSingleton()
#define ACTIONMANAGER actionManager::getSingleton()


//안전하게 삭제 : 매크로 함수 (macro function)
#define SAFE_DELETE(p)			{ if(p) { delete(p);		(p) = NULL; } }
#define SAFE_DELETE_ARRAY(p)	{ if(p) { delete[] (p);		(p) = NULL; } }
#define SAFE_RELEASE(p)			{ if(p) { (p)->release();	(p)	= NULL; } }


//=================================================
//20140611 ## 전역변수 써준다 ## Thesday of Blood
//=================================================
extern HINSTANCE	_hInstance;			//프로그램 인스턴스 (메모리상에 할당되어 실행중인 프로그램)
extern HWND			_hWnd;				//윈도우 핸들
extern POINT		_ptMouse;			//마우스 위치
extern BOOL			_leftButtonDown;

// TODO: 프로그램에 필요한 추가 헤더는 여기에서 참조합니다.
