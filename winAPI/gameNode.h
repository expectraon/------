#pragma once

#include "image.h"

static image* _backBuffer = IMAGEMANAGER->addImage("backBuffer", WINSIZEX, WINSIZEY);

class gameNode
{
private:
	HDC _hdc;
	bool _managerInit;

public:
	virtual HRESULT init(void);
	virtual HRESULT init(bool managerInit);
	virtual void release(void);
	virtual void update(void);
	virtual void render(void);

	//백버퍼 접근자
	image* getBackBuffer(void) {return _backBuffer;}
	HDC getMemDC(void) {return _backBuffer->getMemDC();}
	HDC getHDC() {return _hdc;}

	LRESULT CALLBACK MainProc(HWND hWnd, UINT iMessage, WPARAM wParam, LPARAM lParam);

	gameNode(void);
	virtual ~gameNode(void);
};
