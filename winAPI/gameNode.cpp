#include "StdAfx.h"
#include "gameNode.h"

gameNode::gameNode(void)
{
}

gameNode::~gameNode(void)
{
}

HRESULT gameNode::init(void)
{
	_hdc = GetDC(_hWnd);
	_managerInit = false;
	return S_OK;
}

HRESULT gameNode::init(bool managerInit)
{
	_hdc = GetDC(_hWnd);
	_managerInit = managerInit;

	if (_managerInit)
	{
		KEYMANAGER->init();				//키매니져 초기화
		IMAGEMANAGER->init();			//이미지매니져 초기화
		TIMEMANAGER->init();			//타임매니져 초기화
		TXTMANAGER->init();				//txt매니져 초기화
		SCENEMANAGER->init();			//씬매니져 초기화
		DATABASE->init();				//db 초기화
		SOUNDMANAGER->init();			//사운드매니져 초기화
		EFFECTMANAGER->init();			//이펙트 매니져 초기화
		FAMANAGER->init();					//프레임 애니메이션매니져 초기화
		KEYANIMANAGER->init();			//키프레임애니메이션 초기화
		ACTIONMANAGER->init();			//액션 메니져 초기화
	}

	return S_OK;
}

void gameNode::release(void)
{
	if (_managerInit)
	{
		KEYMANAGER->release();
		KEYMANAGER->releaseSingleton();
		IMAGEMANAGER->release();
		IMAGEMANAGER->releaseSingleton();
		TIMEMANAGER->release();
		TIMEMANAGER->releaseSingleton();
		TXTMANAGER->release();
		TXTMANAGER->releaseSingleton();
		SCENEMANAGER->release();
		SCENEMANAGER->releaseSingleton();
		DATABASE->release();
		DATABASE->releaseSingleton();
		SOUNDMANAGER->release();
		SOUNDMANAGER->releaseSingleton();
		EFFECTMANAGER->release();
		EFFECTMANAGER->releaseSingleton();
		FAMANAGER->release();
		FAMANAGER->releaseSingleton();
		KEYANIMANAGER->release();
		KEYANIMANAGER->releaseSingleton();
		ACTIONMANAGER->release();
		ACTIONMANAGER->releaseSingleton();
	}

	//DC삭제
	ReleaseDC(_hWnd, _hdc);
}

void gameNode::update(void)
{
}

void gameNode::render(void)
{

}

LRESULT CALLBACK gameNode::MainProc(HWND hWnd, UINT iMessage, WPARAM wParam, LPARAM lParam)
{
	switch (iMessage)
	{
		case WM_MOUSEMOVE:
			_ptMouse.x = static_cast<float>LOWORD(lParam);
			_ptMouse.y = static_cast<float>HIWORD(lParam);
		break;
		case WM_LBUTTONDOWN:
			_leftButtonDown = true;
		break;
		case WM_LBUTTONUP:
			_leftButtonDown = false;
		break;
		case WM_KEYDOWN:
			switch (wParam)
			{
				case VK_ESCAPE:
					PostMessage(_hWnd, WM_DESTROY, 0, 0);
				break;
			}				
		break;
		case WM_DESTROY:
			PostQuitMessage(0); //종료 함수
		return 0;
	}

	return (DefWindowProc(hWnd, iMessage, wParam, lParam));
}


