#pragma once

#include "gamenode.h"
#include "tankMap.h"

enum TANKDIRECTION
{
	TANKDIRECTION_NULL = 0,
	TANKDIRECTION_LEFT,
	TANKDIRECTION_UP,
	TANKDIRECTION_RIGHT,
	TANKDIRECTION_DOWN
};

class tank : public gameNode
{
private:
	TANKDIRECTION _TANKDIRECTION;

	tankMap* _tankMap;
	
	image* _image;
	RECT _rc;
	float _x, _y;
	float _speed;

public:
	virtual HRESULT init(void);
	virtual void release(void);
	virtual void update(void);
	virtual void render(void);

	void tankMove(void);
	void setTankPosition(void);

	void setTankMap(tankMap* map) {_tankMap = map;}

	tank(void);
	~tank(void);
};
