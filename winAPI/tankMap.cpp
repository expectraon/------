#include "StdAfx.h"
#include "tankMap.h"

tankMap::tankMap(void)
{
}

tankMap::~tankMap(void)
{
}

HRESULT tankMap::init(void)
{
	gameNode::init();

	//타일맵 로딩
	IMAGEMANAGER->addFrameImage("tileMap", "tileMap.bmp", 0, 0, 640, 288, 20, 9);

	load();
	
	return S_OK;
}

void tankMap::release(void)
{
	gameNode::release();
}

void tankMap::update(void)
{
	gameNode::update();
}

void tankMap::render(void)
{
	//지형출력
	for (int i = 0; i < TILEX * TILEY; i++)
	{
		IMAGEMANAGER->frameRender("tileMap", getMemDC(),
			_map[i].rcTile.left, _map[i].rcTile.top,
			_map[i].terrainFrameX, _map[i].terrainFrameY);
	}

	//오브젝트 출력
	for (int i = 0; i < TILEX * TILEY; i++)
	{
		if (_map[i].obj == OBJ_NONE) continue;

		IMAGEMANAGER->frameRender("tileMap", getMemDC(),
			_map[i].rcTile.left, _map[i].rcTile.top,
			_map[i].objFrameX, _map[i].objFrameY);
	}
}

void tankMap::load(void)
{
	HANDLE file;
	DWORD read;

	file = CreateFile("tileMap.map", GENERIC_READ, 0, NULL,
		OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, NULL);
	ReadFile(file, _map, sizeof(tagMap) * TILEX * TILEY, &read, NULL);
	ReadFile(file, _pos, sizeof(int) * 2, &read, NULL);

	//탱크 블럭 제끼기
	for (int i = 0; i < TILEX * TILEY; i++)
	{
		if (_map[i].obj == OBJ_TANK1 || _map[i].obj == OBJ_TANK2)
		{
			_map[i].obj = OBJ_NONE;
		}
	}

	//속성정의
	memset(_attribute, 0, sizeof(DWORD) * TILEX * TILEY);
	for (int i = 0; i < TILEX * TILEY; i++)
	{
		if (_map[i].terrain == TR_WATER) 
		{
			_attribute[i] |= ATTR_UNMOVAL;
		}
		if (_map[i].obj == OBJ_BLOCK1) _attribute[i] |= ATTR_UNMOVAL;
		if (_map[i].obj == OBJ_BLOCK3) _attribute[i] |= ATTR_UNMOVAL;
		if (_map[i].obj == OBJ_BLOCKS) _attribute[i] |= ATTR_UNMOVAL;
	}

	CloseHandle(file);
}