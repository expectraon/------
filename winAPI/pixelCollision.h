#pragma once
#include "gamenode.h"

class pixelCollision : public gameNode
{
private:
	image* _testImage;
	image* _ball;

	RECT _rc;

	float _x;
	float _y;

	int _probeY;

	bool _collision;

public:
	virtual HRESULT init(void);
	virtual void release(void);
	virtual void update(void);
	virtual void render(void);

	pixelCollision(void);
	~pixelCollision(void);
};
