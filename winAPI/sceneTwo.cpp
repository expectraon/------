#include "StdAfx.h"
#include "sceneTwo.h"

sceneTwo::sceneTwo(void)
{
}

sceneTwo::~sceneTwo(void)
{
}

HRESULT sceneTwo::init(void)
{
	gameNode::init();

	_y = 0;

	wsprintf(_text, "����� SceneTwo��!!");
	
	return S_OK;
}

void sceneTwo::release(void)
{
	gameNode::release();
}

void sceneTwo::update(void)
{
	gameNode::update();

	_y++;
}

void sceneTwo::render(void)
{
	IMAGEMANAGER->findImage("background")->loopRender(getMemDC(), 
		&RectMake(0, 0, WINSIZEX, WINSIZEY), 0, _y);

	TextOut(getMemDC(), WINSIZEX / 2, WINSIZEY / 2, _text, strlen(_text));
}

