#include "StdAfx.h"
#include "tank.h"

tank::tank(void)
{
}

tank::~tank(void)
{
}

HRESULT tank::init(void)
{
	gameNode::init();
	
	_TANKDIRECTION = TANKDIRECTION_NULL;

	_image = IMAGEMANAGER->findImage("땅크");
	_speed = 2.0f;

	return S_OK;
}

void tank::release(void)
{
	gameNode::release();
}

void tank::update(void)
{
	gameNode::update();

	if (KEYMANAGER->isStayKeyDown(VK_LEFT))
	{
		_TANKDIRECTION = TANKDIRECTION_LEFT;
		tankMove();
	}
	else if (KEYMANAGER->isStayKeyDown(VK_RIGHT))
	{
		_TANKDIRECTION = TANKDIRECTION_RIGHT;
		tankMove();
	}
	else if (KEYMANAGER->isStayKeyDown(VK_UP))
	{
		_TANKDIRECTION = TANKDIRECTION_UP;
		tankMove();
	}
	else if (KEYMANAGER->isStayKeyDown(VK_DOWN))
	{
		_TANKDIRECTION = TANKDIRECTION_DOWN;
		tankMove();
	}
}

void tank::render(void)
{
	_image->frameRender(getMemDC(), _rc.left, _rc.top);
}

//탱크 이동
void tank::tankMove(void)
{
	RECT rcCollision; //먼저 보내서 검사할 충돌 렉트
	int tileIndex[2]; //검사할 타일
	int tileX, tileY;	//지금 탱크가 밟고 있는 타일 x, y

	rcCollision = _rc;

	switch (_TANKDIRECTION)
	{
		case TANKDIRECTION_LEFT:
			_image->setFrameX(0);
			_image->setFrameY(2);
			_x -= _speed;
			rcCollision = RectMakeCenter(_x, _y, _image->getFrameWidth() - 4, _image->getFrameHeight() - 4);
		break;
		case TANKDIRECTION_RIGHT:
			_image->setFrameX(3);
			_image->setFrameY(3);
			_x += _speed;
			rcCollision = RectMakeCenter(_x, _y, _image->getFrameWidth() - 4, _image->getFrameHeight() - 4);
		break;
		case TANKDIRECTION_UP:
			_image->setFrameX(0);
			_image->setFrameY(0);
			_y -= _speed;
			rcCollision = RectMakeCenter(_x, _y, _image->getFrameWidth() - 4, _image->getFrameHeight() - 4);
		break;
		case TANKDIRECTION_DOWN:
			_image->setFrameX(0);
			_image->setFrameY(1);
			_y += _speed;
			rcCollision = RectMakeCenter(_x, _y, _image->getFrameWidth() - 4, _image->getFrameHeight() - 4);
		break;
	}

	//지금 밟고 있는 타일 번호 알아온다
	tileX = rcCollision.left / TILESIZE;
	tileY = rcCollision.top / TILESIZE;

	//검사할 타일 인덱스 집어 넣는다
	switch (_TANKDIRECTION)
	{
		case TANKDIRECTION_LEFT:
			tileIndex[0] = tileX + tileY * TILEX;
			tileIndex[1] = tileX + (tileY + 1) * TILEX;
		break;
		case TANKDIRECTION_RIGHT:
			tileIndex[0] = (tileX + tileY * TILEX) + 1;
			tileIndex[1] = (tileX + (tileY + 1) * TILEX) + 1;
		break;
		case TANKDIRECTION_UP:
			tileIndex[0] = tileX + tileY * TILEX;
			tileIndex[1] = tileX + 1 + tileY * TILEX;
		break;
		case TANKDIRECTION_DOWN:
			tileIndex[0] = (tileX + tileY * TILEX) + TILEX;
			tileIndex[1] = (tileX + 1 + tileY * TILEX) + TILEX;
		break;
	}

	for (int i = 0; i < 2; i++)
	{
		//이동불가 속성이고 충돌되었을때...
		RECT rc;
		if ((_tankMap->getAttribute()[tileIndex[i]] & ATTR_UNMOVAL) &&
			IntersectRect(&rc, &_tankMap->getMap()[tileIndex[i]].rcTile,
			&rcCollision))
		{
			switch (_TANKDIRECTION)
			{
				case TANKDIRECTION_LEFT:
					_rc.left = _tankMap->getMap()[tileIndex[i]].rcTile.right;
					_rc.right = _rc.left + 30;
					_x = _rc.left + (_rc.right - _rc.left) / 2;
				break;
				case TANKDIRECTION_RIGHT:
					_rc.right = _tankMap->getMap()[tileIndex[i]].rcTile.left;
					_rc.left = _rc.right - 30;
					_x = _rc.left + (_rc.right - _rc.left) / 2;
				break;
				case TANKDIRECTION_UP:
					_rc.top = _tankMap->getMap()[tileIndex[i]].rcTile.bottom;
					_rc.bottom = _rc.top + 30;
					_y = _rc.top + (_rc.bottom - _rc.top) / 2;
				break;
				case TANKDIRECTION_DOWN:
					_rc.bottom = _tankMap->getMap()[tileIndex[i]].rcTile.top;
					_rc.top = _rc.bottom - 30;
					_y = _rc.top + (_rc.bottom - _rc.top) / 2;
				break;
			}
			return;
		}
	}

	//움직인다
	rcCollision = RectMakeCenter(_x, _y, _image->getFrameWidth() - 4,
		_image->getFrameHeight() - 4);
	_rc = rcCollision;
}

void tank::setTankPosition(void)
{
	_rc = _tankMap->getMap()[_tankMap->getPosFirst()].rcTile;
	_x = _rc.left + (_rc.right - _rc.left) / 2;
	_y = _rc.top + (_rc.bottom - _rc.top) / 2;
}
