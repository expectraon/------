#include "StdAfx.h"
#include "player.h"
#include "enemyManager.h"

player::player(void)
{
}

player::~player(void)
{
}

HRESULT player::init(void)
{
	gameNode::init();

//	_currentHP = 5000;
//	_maxHP = 5000;

	vector<string> vLoad = TXTMANAGER->txtLoad("�ѿ�.txt");

	const char* currentHP = vLoad[0].c_str();
	const char* maxHP = vLoad[1].c_str();

	_currentHP = atoi(currentHP);
	_maxHP = atoi(maxHP);

	_image = IMAGEMANAGER->addFrameImage("ufo", "ufo.bmp", WINSIZEX / 2,
		WINSIZEY / 2 + 200, 3912, 121, 24, 1, true, RGB(255, 0, 255));

	_rc = RectMakeCenter(_image->getCenterX(), _image->getCenterY(), 163, 121);

	_missile = new missileM1;
	_missile->init(20, 600);

	_hpBar = new progressBar;
	_hpBar->init(_rc.left, _rc.top, 163, 5);
	_hpBar->setGauge(_currentHP, _maxHP);

	_getTickCount = GetTickCount();
	_worldTimeCount = 0.0f;
/*
	vector<string> vSave;
	vSave.push_back("����");
	vSave.push_back("���̽�");
	vSave.push_back("���߳�");
	vSave.push_back("�̼ҳ�");
	vSave.push_back("�̿���");
	vSave.push_back("��ƾ");
	vSave.push_back("���嵹�ƿ�");
	vSave.push_back("��¯");
	vSave.push_back("�ѿ�~~��~~��");

	TXTMANAGER->txtSave("�׳�ʹܵ���.avi", vSave);

	vSave.clear();

	vector<string> vLoad;
	vLoad = TXTMANAGER->txtLoad("��ƾ�͵�.txt");
	*/
	return S_OK;
}

void player::release(void)
{
	gameNode::release();
}

void player::update(void)
{
	gameNode::update();

	//����~~~~
	if (KEYMANAGER->isOnceKeyDown('S'))
	{
		char temp[32];
		vector<string> vStr;
		vStr.push_back(itoa(_currentHP, temp, 10));
		vStr.push_back(itoa(_maxHP, temp, 10));

		TXTMANAGER->txtSave("�ѿ�.txt", vStr);
	}

	if (KEYMANAGER->isStayKeyDown(VK_LEFT) && (_image->getX() > 0))
	{
		_image->setX(_image->getX() - 3);
	}
	if (KEYMANAGER->isStayKeyDown(VK_RIGHT) && 
		((_image->getX() + _image->getFrameWidth()) < WINSIZEX))
	{
		_image->setX(_image->getX() + 3);
	}
	if (KEYMANAGER->isStayKeyDown(VK_UP) && (_image->getY() > 0))
	{
		_image->setY(_image->getY() - 3);
	}
	if (KEYMANAGER->isStayKeyDown(VK_DOWN) && 
		((_image->getY() + _image->getFrameHeight()) < WINSIZEY))
	{
		_image->setY(_image->getY() + 3);
	}

	//���̾�~
	if (KEYMANAGER->isOnceKeyDown(VK_SPACE))
	{
		_missile->fire(_image->getX() + (_image->getFrameWidth() / 2), _image->getY());
	}

	//������ �� ������Ʈ
	_hpBar->setGauge(_currentHP, _maxHP);
	_hpBar->setX(_rc.left);
	_hpBar->setY(_rc.top);

	/*
	//�ִϸ��̼� ī��Ʈ1 (GetTickCount())
	if (WORLDTIMECOUNT + _worldTimeCount <= GetTickCount())
	{
		_worldTimeCount = GetTickCount();
		_image->setFrameX(_image->getFrameX() + 1);
		if (_image->getFrameX() >= _image->getMaxFrameX())
		{
			_image->setFrameX(0);
		}
	}*/

	//�ִϸ��̼� ī��Ʈ2 (TIMEMANAGER->getWorldTime())
	if (WORLDTIMECOUNT + _worldTimeCount <= TIMEMANAGER->getWorldTime())
	{
		_worldTimeCount = TIMEMANAGER->getWorldTime();
		_image->setFrameX(_image->getFrameX() + 1);
		if (_image->getFrameX() >= _image->getMaxFrameX())
		{
			_image->setFrameX(0);
		}
	}

	/*
	_count++;
	if (_count % 3 == 0)
	{
		if (_image->getFrameX() >= _image->getMaxFrameX())
		{
			_image->setFrameX(0);
		}

		_image->setFrameX(_image->getFrameX() + 1);
		_count = 0;
	}
*/
	_rc = RectMakeCenter(_image->getCenterX(), _image->getCenterY(), 163, 121);

	_missile->update();

	//�ڰ� �浹
//	collision();
}

void player::render(void)
{
	_image->frameRender(getMemDC());
	_missile->render();

	_hpBar->render();
}

void player::damage(int dam)
{
	_currentHP -= dam;	
}

void player::removeMissile(int arrNum)
{
	_missile->removeMissile(arrNum);
}

//������ �浹üũ
void player::collision(void)
{
	if (_enemyManager == NULL) return;

	for (int i = 0; i < _missile->getVBullet().size(); i++)
	{
		for (int j = 0; j < _enemyManager->getVMinion().size(); j++)
		{
			RECT rc;
			if (IntersectRect(&rc, &_missile->getVBullet()[i].rc, 
				&_enemyManager->getVMinion()[j]->getRect()))
			{
				this->removeMissile(i);
				_enemyManager->removeMinion(j);
				break;
			}
		}
	}
}
