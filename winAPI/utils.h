#pragma once

#include <cmath>

#define DEG_TO_RAD 0.017453f
#define PI ((FLOAT)3.141592654f)
#define PI2 6.283185f
#define PI4 (PI * 4)
#define PI_4 float(PI / 4.0f)
#define PI_8 float(PI / 8.0f)
#define PI_16 float(PI / 16.0f)
#define PI_32 float(PI / 32.0f)
#define PI_64 float(PI / 64.0f)
#define PI_128 float(PI / 128.0f)

#define FLOAT_EPSILON 0.001f
#define FLOAT_TO_INT(f1) static_cast<int>(f1 + FLOAT_EPSILON)
#define FLOAT_EQUAL(f1, f2) (fabs(f1 - f2) <= FLOAT_EPSILON)
//fabs는 소수점 차의 절대값을 반환하는 함수이다.

//유틸 네임스페이쓰~
namespace MY_UTIL
{
	//start로부터 end까지의 각을 라디안으로 구한당..
	float getAngle(float startX, float startY, float endX, float endY);

	//거리 구한당
	float getDistance(float startX, float startY, float endX, float endY);
}
