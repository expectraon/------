#pragma once
#include "gamenode.h"
#include "tankMap.h"
#include "tank.h"

class tankGameScene : public gameNode
{
private:
	tankMap* _tankMap;
	tank* _tank;

public:
	virtual HRESULT init(void);
	virtual void release(void);
	virtual void update(void);
	virtual void render(void);

	tankGameScene(void);
	~tankGameScene(void);
};
