#pragma once

#include "gamenode.h"
#include "bullets.h"
#include "progressBar.h"
//#include "enemyManager.h"

//전방선언
class enemyManager;

#define WORLDTIMECOUNT 0.0001f

class player : public gameNode
{
private:
	RECT _rc;
	image* _image;
	missileM1* _missile;
	progressBar* _hpBar;

	int _count;

	int _currentHP;
	int _maxHP;

	float _worldTimeCount;
	float _getTickCount;

	enemyManager* _enemyManager;

public:
	virtual HRESULT init(void);
	virtual void release(void);
	virtual void update(void);
	virtual void render(void);
	virtual void collision(void);

	virtual void removeMissile(int arrNum);

	missileM1* getMissile(void) {return _missile;}
	image* getImage(void) {return _image;}
	RECT getRect(void) {return _rc;}
	
	//에너찌~~
	void damage(int dam);

	void setEmMemoryAddressLink(enemyManager* em) {_enemyManager = em;}

	player(void);
	virtual ~player(void);
};
