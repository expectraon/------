#include "StdAfx.h"
#include "actionManager.h"
#include "action.h"

actionManager::actionManager(void)
{
}

actionManager::~actionManager(void)
{
}

HRESULT actionManager::init(void)
{
	return S_OK;
}

void actionManager::release(void)
{
	for (_viAction = _vAction.begin(); _viAction != _vAction.end();)
	{
		if (_vAction.size() != 0)
		{
			SAFE_DELETE(*_viAction);
			_viAction = _vAction.erase(_viAction);
		}
		else ++_viAction;
	}

	_vAction.clear();
}

void actionManager::update(void)
{
	for (int i = 0; i < _vAction.size(); i++)
	{
		_vAction[i]->update();
		if (!_vAction[i]->getInMoving())
		{
			SAFE_DELETE(_vAction[i]);
			_vAction.erase(_vAction.begin() + i);
			break;
		}
	}
}

void actionManager::render(void)
{
}

void actionManager::moveTo(image* img, float endX, float endY, float time)
{
	action* moveAction = new action;
	moveAction->init();
	moveAction->moveTo(img, endX, endY, time);
	_vAction.push_back(moveAction);
}

void actionManager::moveTo(image* img, float endX, float endY, float time, void* cbFunction)
{
	action* moveAction = new action;
	moveAction->init();
	moveAction->moveTo(img, endX, endY, time, (CALLBACK_FUNCTION)cbFunction);
	_vAction.push_back(moveAction);
}

void actionManager::moveTo(image* img, float endX, float endY, float time, void* cbFunction, void* obj)
{
	action* moveAction = new action;
	moveAction->init();
	moveAction->moveTo(img, endX, endY, time, (CALLBACK_FUNCTION_PARAMETER)cbFunction, obj);
	_vAction.push_back(moveAction);
}


