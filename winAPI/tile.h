#pragma once

#include "gamenode.h"
#include <string>

class tile : public gameNode
{
private:
	int _idX;
	int _idY;

	POINT _center;
	RECT _rc;

	float _totalCost;			//f : g + h의 값
	float _costFromStart; //g : 시작점부터 현재 노드까지의 경로 비용
	float _costToGoal;		//h : 현재 노드로부터 끝점까지의 경로 비용

	bool _isOpen;
	tile* _parentNode;
	
	COLORREF _color;
	HBRUSH _brush;
	HPEN _pen;

	string _attribute;

public:
	HRESULT init(int idX, int idY);
	void release(void);
	void update(void);
	void render(void);

	void setColor(COLORREF color)
	{
		DeleteObject(_brush);
		_color = color;
		_brush = CreateSolidBrush(_color);
	}

	int getIdX(void) {return _idX;}
	void setIdX(int idX) {_idX = idX;}

	int getIdY(void) {return _idY;}
	void setIdY(int idY) {_idY = idY;}

	float getTotalCost(void) {return _totalCost;}
	void setTotalCost(float cost) {_totalCost = cost;}
	
	float getCostFromStart(void) {return _costFromStart;}
	void setCostFromStart(float cost) {_costFromStart = cost;}

	float getCostToGoal(void) {return _costToGoal;}
	void setCostToGoal(float cost) {_costToGoal = cost;}

	bool getIsOpen(void) {return _isOpen;}
	void setIsOpen(bool isOpen) {_isOpen = isOpen;}

	tile* getParentNode(void) {return _parentNode;}
	void setParentNode(tile* parentNode) {_parentNode = parentNode;}

	string getAttribute(void) {return _attribute;}
	void setAttribute(string attribute) {_attribute = attribute;}

	POINT getCenter(void) {return _center;}
	void setCenter(POINT center) {_center = center;}

	RECT getRect(void) {return _rc;}
	void setRect(RECT rc) {_rc = rc;}

	tile(void);
	~tile(void);
};
