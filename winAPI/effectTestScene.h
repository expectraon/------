#pragma once
#include "gamenode.h"
#include "frameAnimation.h"

class effectTestScene : public gameNode
{
private:
	frameAnimation* _fa1;

public:
	virtual HRESULT init(void);
	virtual void release(void);
	virtual void update(void);
	virtual void render(void);

	effectTestScene(void);
	~effectTestScene(void);
};
