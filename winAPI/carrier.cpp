#include "StdAfx.h"
#include "carrier.h"

carrier::carrier(void)
{
}

carrier::~carrier(void)
{
}

void carrier::keyControl(void)
{
	if (KEYMANAGER->isStayKeyDown(VK_LEFT))
	{
		_angle += 0.05f;
		if (_angle > PI * 2) _angle -= PI * 2;
	}

	if (KEYMANAGER->isStayKeyDown(VK_RIGHT))
	{
		_angle -= 0.05f;
		if (_angle < 0) _angle += PI * 2;
	}

	if (KEYMANAGER->isStayKeyDown(VK_UP))
	{
		_speed += 0.02;
	}
	if (KEYMANAGER->isStayKeyDown(VK_DOWN))
	{
		_speed -= 0.02;
	}
}