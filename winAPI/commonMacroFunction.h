﻿#pragma once
#include <vector>

//==============================================
//20140612 ## commonMacroFunction ##
//==============================================

//point
inline POINT PointMake(int x, int y)
{
	POINT pt = {x, y};
	return pt;
}

//line
inline void LineMake(HDC hdc, int x1, int y1, int x2, int y2)
{
	MoveToEx(hdc, x1, y1, NULL);
	LineTo(hdc, x2, y2);
}

inline void LineMake(HDC hdc, std::vector<POINT> vPt)
{
	std::vector<POINT>::iterator viPoint;
	int i = 0;

	for (viPoint = vPt.begin(); viPoint != vPt.end(); ++viPoint, i++)
	{
		if (i == 0) MoveToEx(hdc, viPoint->x, viPoint->y, NULL);
		else LineTo(hdc, viPoint->x, viPoint->y);
	}
	vPt.clear();
}

//rect
inline RECT RectMake(int x, int y, int width, int height)
{
	RECT rc = {x, y, x + width, y + height};
	return rc;
}

inline RECT RectMakeCenter(int x, int y, int width, int height)
{
	RECT rc = {x - width / 2, y - height / 2, x + width / 2, y + height / 2};
	return rc;
}

inline void RectangleMake(HDC hdc, int x, int y, int width, int height)
{
	Rectangle(hdc, x - width / 2, y - height / 2, x + width / 2, y + height / 2);
}

//ellipse
inline RECT EllipMake(int x, int y, int width, int height)
{
	RECT rc = {x, y, x + width, y + height};
	return rc;
}

inline RECT EllipMakeCenter(int x, int y, int width, int height)
{
	RECT rc = {x - width / 2, y - height / 2, x + width / 2, y + height / 2};
	return rc;
}

inline void EllipseMake(HDC hdc, int x, int y, int width, int height)
{
	Ellipse(hdc, x - width / 2, y - height / 2, x + width / 2, y + height / 2);
}

inline RECT collisionAreaResizing(RECT &rcDest, int width, int height)
{
	RECT rc = {rcDest.left + width / 2, rcDest.top + height / 2,
		rcDest.right - width / 2, rcDest.bottom - height / 2};
	return rc;
}