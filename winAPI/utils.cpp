#include "StdAfx.h"
#include "utils.h"

namespace MY_UTIL
{

	float getAngle(float startX, float startY, float endX, float endY)
	{
		float x = endX - startX;
		float y = endY - startY;

		float d = sqrtf(x * x + y * y);

		float angle = acos(x / d);
		if (y > 0) angle = PI2 - angle;

		return angle;
	}

	//거리 구한당
	float getDistance(float startX, float startY, float endX, float endY)
	{
		float x = endX - startX;
		float y = endY - startY;

		return sqrtf(x * x + y * y);
	}
}