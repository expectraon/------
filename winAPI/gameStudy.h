#pragma once

#include "gamenode.h"
#include "ghotsnGoblinsScene.h"
#include "aStarTestScene.h"
#include "pixelCollisionScene.h"
#include "actionTestScene.h"
#include "loadingScene.h"
#include "memoryLeakTestScene.h"
#include "sadarimain.h"

using namespace std;


class gameStudy : public gameNode
{
private:

public:
	virtual HRESULT init(void);
	virtual void release(void);
	virtual void update(void);
	virtual void render(void);

	gameStudy(void);
	virtual ~gameStudy(void);
};
