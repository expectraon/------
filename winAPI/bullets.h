#pragma once

#include "gamenode.h"
#include <vector>

struct tagBullet
{
	image* img;
	RECT rc;
	int radius;
	float speed;
	float x;
	float y;
	float fireX;
	float fireY;
	float angle;
	int count;
	bool fire;
};

//공용으로 쓰는 총알 (쏠때마다 만들고 삭제..)
class bullet : public gameNode
{
private:
	vector<tagBullet> _vBullet;
	vector<tagBullet>::iterator _viBullet;

	const char* _imageName;
	float _range;
	float _bulletMax;

public:
	virtual HRESULT init(const char* imageName, int bulletMax, float range);
	virtual void release(void);
	virtual void update(void);
	virtual void render(void);

	void fire(float x, float y, float angle, float speed);
	void move(void);
	void draw(void);

	void removeBullet(int arrNum);

	vector<tagBullet> getVBullet(void) {return _vBullet;}
	vector<tagBullet>::iterator getVIBullet(void) {return _viBullet;}

	bullet(void);
	virtual ~bullet(void);
};

//미리 만들어 놓고 쓰는 미사일
class missile : public gameNode
{
private:
	vector<tagBullet> _vBullet;
	vector<tagBullet>::iterator _viBullet;

	float _range;

public:
	virtual HRESULT init(int bulletMax, float range);
	virtual void release(void);
	virtual void update(void);
	virtual void render(void);

	void fire(float x, float y);
	void move(void);
	void draw(void);

	missile(void);
	virtual ~missile(void);
};

//쏠때마다 생성해서 쓰는 미사일
class missileM1 : public gameNode
{
private:
	vector<tagBullet> _vBullet;
	vector<tagBullet>::iterator _viBullet;

	float _range;
	int _bulletMax;

public:
	virtual HRESULT init(int bulletMax, float range);
	virtual void release(void);
	virtual void update(void);
	virtual void render(void);

	void fire(float x, float y);
	void move(void);
	void draw(void);

	void removeMissile(int arrNum);

	vector<tagBullet> getVBullet(void) {return _vBullet;}
	vector<tagBullet>::iterator getVIBullet(void) {return _viBullet;}

	missileM1(void);
	virtual ~missileM1(void);
};
