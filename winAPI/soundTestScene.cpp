#include "StdAfx.h"
#include "soundTestScene.h"

soundTestScene::soundTestScene(void)
{
}

soundTestScene::~soundTestScene(void)
{
}

HRESULT soundTestScene::init(void)
{
	gameNode::init();

	IMAGEMANAGER->addImage("venusOnEarth", "venusOnEarth.bmp", WINSIZEX, WINSIZEY, true, RGB(255, 0, 255));

	_btnPlay = new button;
	_btnPlay->init("button", WINSIZEX / 2 - 260, WINSIZEY / 2 - 240, 
		PointMake(0, 1), PointMake(0, 0), cbPlay);

	_btnStop = new button;
	_btnStop->init("button", WINSIZEX / 2 - 260, WINSIZEY / 2 - 140, 
		PointMake(0, 1), PointMake(0, 0), cbStop);

	_btnPause = new button;
	_btnPause->init("button", WINSIZEX / 2 - 260, WINSIZEY / 2 - 40, 
		PointMake(0, 1), PointMake(0, 0), cbPause);

//	SOUNDMANAGER->setUp("Dengue Fever - Integration.mp3", 0, true, true);

	SOUNDMANAGER->addSound("music", "Dengue Fever - Integration.mp3", true, true);

	return S_OK;
}

void soundTestScene::release(void)
{
	gameNode::release();

	_btnPlay->release();
	SAFE_DELETE(_btnPlay);

	_btnStop->release();
	SAFE_DELETE(_btnStop);

	_btnPause->release();
	SAFE_DELETE(_btnPause);
}

void soundTestScene::cbPlay(void)
{
	if (!SOUNDMANAGER->isPlaySound("music"))
	{
		SOUNDMANAGER->play("music", 1.0f);
	}
	else SOUNDMANAGER->stop("music");
}

void soundTestScene::cbStop(void)
{
	if (SOUNDMANAGER->isPlaySound("music"))
	{
		SOUNDMANAGER->stop("music");
	}
}

void soundTestScene::cbPause(void)
{
	if (!SOUNDMANAGER->isPauseSound("music"))
	{
		SOUNDMANAGER->pause("music");
	}
	else SOUNDMANAGER->resume("music");
}

void soundTestScene::update(void)
{
	gameNode::update();

	_btnPlay->update();
	_btnStop->update();
	_btnPause->update();

	SOUNDMANAGER->update();
}

void soundTestScene::render(void)
{
	IMAGEMANAGER->findImage("venusOnEarth")->render(getMemDC(), 0, 0);

	_btnPlay->render();
	_btnStop->render();
	_btnPause->render();
}